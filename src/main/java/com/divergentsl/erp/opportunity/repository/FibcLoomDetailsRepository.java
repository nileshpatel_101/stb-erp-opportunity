package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcLoomDetailsDocument;

@Repository
public interface FibcLoomDetailsRepository extends MongoRepository<FibcLoomDetailsDocument, String>{

}
