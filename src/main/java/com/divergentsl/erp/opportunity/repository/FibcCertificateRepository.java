package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcCertificate;

@Repository
public interface FibcCertificateRepository extends MongoRepository<FibcCertificate, String>
{

}
