package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.divergentsl.erp.opportunity.mongodb.documents.BeltOrderSpecificationDocument;

@Repository
public interface BeltOrderSpecificationRepository extends MongoRepository<BeltOrderSpecificationDocument, String> {

}
