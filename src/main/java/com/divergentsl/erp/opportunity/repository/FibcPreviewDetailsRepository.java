package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcPreviewDetails;

public interface FibcPreviewDetailsRepository extends MongoRepository<FibcPreviewDetails, String>{

}
