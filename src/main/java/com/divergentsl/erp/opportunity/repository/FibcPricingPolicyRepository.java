package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcPricingPolicy;
@Repository
public interface FibcPricingPolicyRepository extends MongoRepository<FibcPricingPolicy,String>{

}
