package com.divergentsl.erp.opportunity.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcModel;

@Repository
public interface FibcModelRepository extends MongoRepository<FibcModel, String> {

	
	//@Query("{epc_values.epc : ?0}")
}//(value="check")