package com.divergentsl.erp.opportunity.constants;

public class UrlConstants {

	private UrlConstants(){
		
	}
	// OPPORTUNITY DETAILS
	public static final String GET_ALL_ENQUIRY = "/get_all_enquiry";
	public static final String GET_ALL_FIBC_MODEL = "/get_all_fibc_model";
	public static final String GET_TOTAL_WEIGHT_AND_COST_FOR_ENQUIRY = "/get_total_cost_and_weight_for_enquiry";
	public static final String GET_CURRENCY_RATE = "get_crrency_rate";
	public static final String SAVE_ENQUIRY = "/save_enquiry";
	public static final String CHECK_CERTIFICATE_ENQUIRY = "check_certificate_enquiry";
	public static final String SAVE_FIBC_MODEL = "/save_fibc_model";
	public static final String GET_ENQUIRY_BY_ENQUIRY_ID = "/get_enquiry_by_enquiry_id";
	public static final String SAVE_FIBC_PREVIEW_DETAILS = "save_fibc_preview_details";
	public static final String GET_FIBC_PREVIEW_DETAILS = "get_fibc_preview_details";
}
