package com.divergentsl.erp.opportunity.exceptions;

public class OpportunityValidationException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	private String message;
	private Long code;

	public OpportunityValidationException(String message) {
		super(message);
		this.message = message;
	}

	public OpportunityValidationException(String message, Throwable t) {
		super(message, t);
		this.message = message;
	}

	public OpportunityValidationException(String message, Long code) {
		super(message);
		this.message = message;
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

}
