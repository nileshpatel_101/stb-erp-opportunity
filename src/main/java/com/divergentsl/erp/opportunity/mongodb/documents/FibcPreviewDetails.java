package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.domains.FibcQuotationAttributes;
import com.divergentsl.erp.opportunity.domains.LoopStyleProperties;

@Document(collection = "fibc_preview_details")
public class FibcPreviewDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String FIBC_PREVIEW_ID = "id";
	public static final String FIBC_PREVIEW_JSON = "json";
	public static final String FIBC_PREVIEW_DIMENSION = "dimension";
	public static final String FIBC_PREVIEW_LOOP_TYPE = "loop_type";
	public static final String FIBC_PREVIEW_ACCESSORIES = "accessories";

	@Id
	@Field(FIBC_PREVIEW_ID)
	private String id;

	@Field(FIBC_PREVIEW_JSON)
	private String json;

	@Field(FIBC_PREVIEW_DIMENSION)
	private List<Attributes> dimension;

	@Field(FIBC_PREVIEW_LOOP_TYPE)
	private LoopStyleProperties loopType;

	@Field(FIBC_PREVIEW_ACCESSORIES)
	private List<FibcQuotationAttributes> accessories;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public List<Attributes> getDimension() {
		return dimension;
	}

	public void setDimension(List<Attributes> dimension) {
		this.dimension = dimension;
	}

	public LoopStyleProperties getLoopType() {
		return loopType;
	}

	public void setLoopType(LoopStyleProperties loopType) {
		this.loopType = loopType;
	}

	public List<FibcQuotationAttributes> getAccessories() {
		return accessories;
	}

	public void setAccessories(List<FibcQuotationAttributes> accessories) {
		this.accessories = accessories;
	}

}
