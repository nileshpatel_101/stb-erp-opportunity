package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "belt_order_specification")
public class BeltOrderSpecificationDocument implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CUSTOMER_NAME = "customer_name";
	private static final String SHEET_TYPE = "sheet_type";
	private static final String TYPE_OF_BAG = "type_of_bag";
	private static final String ORDER_NUMBER = "order_number";
	private static final String BAG_SIZE = "bag_size";
	private static final String ORDER_QUANTITY = "order_qunatity";
	private static final String BAG_CAPACITY = "bag_capacity";
	private static final String LOOP_DIAGRAM = "loop_diagram";
	private static final String BELT_COLOR = "belt_color";
	private static final String BELT_GRM = "belt_grm";
	private static final String BELT_WIDTH = "belt_width";
	private static final String BELT_MARKER = "belt_marker";
	private static final String TAPE_DENIER = "tape_denier";
	private static final String TAPE_WIDTH = "tape_width";
	private static final String TAPE_STRENGTH = "tape_strength";
	private static final String TAPE_MARKER = "tape_marker";

	@Field(CUSTOMER_NAME)
	private String customerName;

	@Field(SHEET_TYPE)
	private String sheetType;

	@Field(TYPE_OF_BAG)
	private String typeOfBag;

	@Field(ORDER_NUMBER)
	private String orderNumber;

	@Field(BAG_SIZE)
	private String bagSize;

	@Field(ORDER_QUANTITY)
	private String orderQuantity;

	@Field(BAG_CAPACITY)
	private String bagCapacity;

	@Field(LOOP_DIAGRAM)
	private String loopDiagram;

	@Field(BELT_COLOR)
	private String beltColor;

	@Field(BELT_GRM)
	private String beltGrm;

	@Field(BELT_WIDTH)
	private String beltWidth;

	@Field(BELT_MARKER)
	private String beltMarker;

	@Field(TAPE_DENIER)
	private String tapeDenier;

	@Field(TAPE_WIDTH)
	private String tapeWidth;

	@Field(TAPE_STRENGTH)
	private String tapeStrength;

	@Field(TAPE_MARKER)
	private String tapeMarker;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSheetType() {
		return sheetType;
	}

	public void setSheetType(String sheetType) {
		this.sheetType = sheetType;
	}

	public String getTypeOfBag() {
		return typeOfBag;
	}

	public void setTypeOfBag(String typeOfBag) {
		this.typeOfBag = typeOfBag;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBagSize() {
		return bagSize;
	}

	public void setBagSize(String bagSize) {
		this.bagSize = bagSize;
	}

	public String getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getBagCapacity() {
		return bagCapacity;
	}

	public void setBagCapacity(String bagCapacity) {
		this.bagCapacity = bagCapacity;
	}

	public String getLoopDiagram() {
		return loopDiagram;
	}

	public void setLoopDiagram(String loopDiagram) {
		this.loopDiagram = loopDiagram;
	}

	public String getBeltColor() {
		return beltColor;
	}

	public void setBeltColor(String beltColor) {
		this.beltColor = beltColor;
	}

	public String getBeltGrm() {
		return beltGrm;
	}

	public void setBeltGrm(String beltGrm) {
		this.beltGrm = beltGrm;
	}

	public String getBeltWidth() {
		return beltWidth;
	}

	public void setBeltWidth(String beltWidth) {
		this.beltWidth = beltWidth;
	}

	public String getBeltMarker() {
		return beltMarker;
	}

	public void setBeltMarker(String beltMarker) {
		this.beltMarker = beltMarker;
	}

	public String getTapeDenier() {
		return tapeDenier;
	}

	public void setTapeDenier(String tapeDenier) {
		this.tapeDenier = tapeDenier;
	}

	public String getTapeWidth() {
		return tapeWidth;
	}

	public void setTapeWidth(String tapeWidth) {
		this.tapeWidth = tapeWidth;
	}

	public String getTapeStrength() {
		return tapeStrength;
	}

	public void setTapeStrength(String tapeStrength) {
		this.tapeStrength = tapeStrength;
	}

	public String getTapeMarker() {
		return tapeMarker;
	}

	public void setTapeMarker(String tapeMarker) {
		this.tapeMarker = tapeMarker;
	}

}
