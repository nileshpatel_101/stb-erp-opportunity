package com.divergentsl.erp.opportunity.mongodb.documents;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "currency_rate")
public class CurrencyDetails 
{
public static final String CURRENCY_RATE="currency_rate";

@Field(CURRENCY_RATE)
private String currencyRate;

public String getCurrencyRate() {
	return currencyRate;
}

public void setCurrencyRate(String currencyRate) {
	this.currencyRate = currencyRate;
}


}
