package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.domains.FibcComponent;
import com.divergentsl.erp.opportunity.domains.LoopStyleProperties;
import com.divergentsl.erp.opportunity.domains.TopBottomStypeProperties;

@Document(collection = "fibc_model")
public class FibcModel implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String FIBC_MODEL_ID = "id";
	public static final String FIBC_MODEL_NAME = "fibc_model_name";
	public static final String FIBC_MODEL_IMAGE_URL = "fibc_model_image_url";
	public static final String FIBC_MODEL_BODY_ATTRIBUTES = "body_attributes";
	public static final String FIBC_MODEL_BOTTOM_ATTRIBUTES = "bottom_attributes";
	public static final String FIBC_MODEL_TIE_ATTRIBUTES = "tie_attributes";
	public static final String FIBC_MODEL_FIBC_ATTRIBUTES = "fibc_attributes";
	public static final String FIBC_MODEL_DIMENSION = "dimension";
	public static final String FIBC_MODEL_LOOPS = "loops";
	public static final String FIBC_MODEL_EXTRA_LOOPS = "extra_loops";
	public static final String FIBC_MODEL_FRONT_STYLE = "front-style";
	public static final String FIBC_MODEL_BOTTOM_STYLE = "bottom-style";
	public static final String FIBC_MODEL_COMPONENT = "components";
	public static final String FIBC_MODEL_ACCESSORIES = "accessories";
	public static final String FIBC_MODEL_PRINTING = "printings";
	public static final String FIBC_MODEL_EXTRAS = "extras";

	@Id
	@Field(FIBC_MODEL_ID)
	private String id;

	@Field(FIBC_MODEL_NAME)
	private String fibcModelName;

	@Field(FIBC_MODEL_IMAGE_URL)
	private String fibcModelImageUrl;

	@Field(FIBC_MODEL_BODY_ATTRIBUTES)
	private List<Attributes> bodyAttributes;

	@Field(FIBC_MODEL_BOTTOM_ATTRIBUTES)
	private List<Attributes> bottomAttributes;

	@Field(FIBC_MODEL_TIE_ATTRIBUTES)
	private List<Attributes> tieAttributes;

	@Field(FIBC_MODEL_FIBC_ATTRIBUTES)
	private List<Attributes> fibcAttributes;

	@Field(FIBC_MODEL_DIMENSION)
	private List<Attributes> dimension;

	@Field(FIBC_MODEL_LOOPS)
	private List<LoopStyleProperties> loops;

	@Field(FIBC_MODEL_EXTRA_LOOPS)
	private List<LoopStyleProperties> extraLoops;

	@Field(FIBC_MODEL_FRONT_STYLE)
	private List<TopBottomStypeProperties> frontStyle;

	@Field(FIBC_MODEL_BOTTOM_STYLE)
	private List<TopBottomStypeProperties> bottomStyle;

	@Field(FIBC_MODEL_COMPONENT)
	private List<FibcComponent> components;

	@Field(FIBC_MODEL_ACCESSORIES)
	private List<FibcComponent> accessories;

	@Field(FIBC_MODEL_PRINTING)
	private List<FibcComponent> printings;

	@Field(FIBC_MODEL_EXTRAS)
	private List<FibcComponent> extras;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFibcModelName() {
		return fibcModelName;
	}

	public void setFibcModelName(String fibcModelName) {
		this.fibcModelName = fibcModelName;
	}

	public String getFibcModelImageUrl() {
		return fibcModelImageUrl;
	}

	public void setFibcModelImageUrl(String fibcModelImageUrl) {
		this.fibcModelImageUrl = fibcModelImageUrl;
	}

	public List<Attributes> getBodyAttributes() {
		return bodyAttributes;
	}

	public void setBodyAttributes(List<Attributes> bodyAttributes) {
		this.bodyAttributes = bodyAttributes;
	}

	public List<Attributes> getBottomAttributes() {
		return bottomAttributes;
	}

	public void setBottomAttributes(List<Attributes> bottomAttributes) {
		this.bottomAttributes = bottomAttributes;
	}

	public List<Attributes> getTieAttributes() {
		return tieAttributes;
	}

	public void setTieAttributes(List<Attributes> tieAttributes) {
		this.tieAttributes = tieAttributes;
	}

	public List<Attributes> getFibcAttributes() {
		return fibcAttributes;
	}

	public void setFibcAttributes(List<Attributes> fibcAttributes) {
		this.fibcAttributes = fibcAttributes;
	}

	public List<Attributes> getDimension() {
		return dimension;
	}

	public void setDimension(List<Attributes> dimension) {
		this.dimension = dimension;
	}

	public List<LoopStyleProperties> getLoops() {
		return loops;
	}

	public void setLoops(List<LoopStyleProperties> loops) {
		this.loops = loops;
	}

	public List<LoopStyleProperties> getExtraLoops() {
		return extraLoops;
	}

	public void setExtraLoops(List<LoopStyleProperties> extraLoops) {
		this.extraLoops = extraLoops;
	}

	public List<TopBottomStypeProperties> getFrontStyle() {
		return frontStyle;
	}

	public void setFrontStyle(List<TopBottomStypeProperties> frontStyle) {
		this.frontStyle = frontStyle;
	}

	public List<TopBottomStypeProperties> getBottomStyle() {
		return bottomStyle;
	}

	public void setBottomStyle(List<TopBottomStypeProperties> bottomStyle) {
		this.bottomStyle = bottomStyle;
	}

	public List<FibcComponent> getComponents() {
		return components;
	}

	public void setComponents(List<FibcComponent> components) {
		this.components = components;
	}

	public List<FibcComponent> getAccessories() {
		return accessories;
	}

	public void setAccessories(List<FibcComponent> accessories) {
		this.accessories = accessories;
	}

	public List<FibcComponent> getPrintings() {
		return printings;
	}

	public void setPrintings(List<FibcComponent> printings) {
		this.printings = printings;
	}

	public List<FibcComponent> getExtras() {
		return extras;
	}

	public void setExtras(List<FibcComponent> extras) {
		this.extras = extras;
	}

}
