package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "fibc-pricing-poicy")
public class FibcPricingPolicy implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String PRICING_POLICY_ID = "id";
	public static final String PRICING_POLICY_COST = "cost";
	public static final String PRICING_POLICY_UPDATED_BY = "updated_by";
	public static final String PRICING_POLICY_START_DATE = "start_date";
	public static final String PRICING_POLICY_END_DATE = "end_date";

	@Id
	@Field(PRICING_POLICY_ID)
	private String id;

	@Field(PRICING_POLICY_COST)
	private double cost;

	@Field(PRICING_POLICY_UPDATED_BY)
	private String updatedBy;

	@Field(PRICING_POLICY_START_DATE)
	private String startDate;

	@Field(PRICING_POLICY_END_DATE)
	private String endDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
