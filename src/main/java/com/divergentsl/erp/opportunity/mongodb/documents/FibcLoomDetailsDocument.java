package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "fibc_loom_details")
public class FibcLoomDetailsDocument implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CUSTOMER_NAME = "customer_name";
	private static final String SHEET_BAG = "sheet_bag";
	private static final String TYPE_OF_BAG = "type_of_bag";
	private static final String ORDER_NUMBER = "order_number";
	private static final String BAG_WEIGHT = "bag_weight";
	private static final String BAG_SIZE = "bag_size";
	private static final String ORDER_QUANTITY = "order_qunatity";
	private static final String FABRIC_WIDTH = "fabric_width";
	private static final String FABRIC_COLOR = "fabric_color";
	private static final String RUNNING_METER = "running_meter";
	private static final String FABRIC_GSM = "fabric_gsm";
	private static final String QUANTITY_OF_FABRIC = "quantity_of_fabric";
	private static final String QUANTITY_IN_STOCK = "quantity_in_stock";
	private static final String NUMBER_OF_MACHINE = "number_of_machine";
	private static final String MARKING_CODE = "marking_code";

	@Field(CUSTOMER_NAME)
	private String customerName;

	@Field(SHEET_BAG)
	private String sheetType;

	@Field(TYPE_OF_BAG)
	private String typeOfBag;

	@Field(ORDER_NUMBER)
	private String orderNumber;

	@Field(BAG_WEIGHT)
	private String bagWeight;

	@Field(BAG_SIZE)
	private String bagSize;

	@Field(ORDER_QUANTITY)
	private String orderQuantity;

	@Field(FABRIC_WIDTH)
	private String fabricWidth;

	@Field(FABRIC_COLOR)
	private String fabricColor;

	@Field(RUNNING_METER)
	private String runningMeter;

	@Field(FABRIC_GSM)
	private String fabricGsm;

	@Field(QUANTITY_OF_FABRIC)
	private String quantityOfFabric;

	@Field(QUANTITY_IN_STOCK)
	private String quantityInStock;

	@Field(NUMBER_OF_MACHINE)
	private String numberOfMachine;

	@Field(MARKING_CODE)
	private String markingCode;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSheetType() {
		return sheetType;
	}

	public void setSheetType(String sheetType) {
		this.sheetType = sheetType;
	}

	public String getTypeOfBag() {
		return typeOfBag;
	}

	public void setTypeOfBag(String typeOfBag) {
		this.typeOfBag = typeOfBag;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBagWeight() {
		return bagWeight;
	}

	public void setBagWeight(String bagWeight) {
		this.bagWeight = bagWeight;
	}

	public String getBagSize() {
		return bagSize;
	}

	public void setBagSize(String bagSize) {
		this.bagSize = bagSize;
	}

	public String getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getFabricWidth() {
		return fabricWidth;
	}

	public void setFabricWidth(String fabricWidth) {
		this.fabricWidth = fabricWidth;
	}

	public String getFabricColor() {
		return fabricColor;
	}

	public void setFabricColor(String fabricColor) {
		this.fabricColor = fabricColor;
	}

	public String getRunningMeter() {
		return runningMeter;
	}

	public void setRunningMeter(String runningMeter) {
		this.runningMeter = runningMeter;
	}

	public String getFabricGsm() {
		return fabricGsm;
	}

	public void setFabricGsm(String fabricGsm) {
		this.fabricGsm = fabricGsm;
	}

	public String getQuantityOfFabric() {
		return quantityOfFabric;
	}

	public void setQuantityOfFabric(String quantityOfFabric) {
		this.quantityOfFabric = quantityOfFabric;
	}

	public String getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(String quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public String getNumberOfMachine() {
		return numberOfMachine;
	}

	public void setNumberOfMachine(String numberOfMachine) {
		this.numberOfMachine = numberOfMachine;
	}

	public String getMarkingCode() {
		return markingCode;
	}

	public void setMarkingCode(String markingCode) {
		this.markingCode = markingCode;
	}

}
