package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.domains.FibcComponent;
import com.divergentsl.erp.opportunity.domains.FibcQuotation;
import com.divergentsl.erp.opportunity.domains.FileThumbnail;
import com.divergentsl.erp.opportunity.domains.LoopStyleProperties;
import com.divergentsl.erp.opportunity.domains.TopBottomStypeProperties;

@Document(collection = "fibc_enquiry")
public class FibcEnquiry implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ENQUIRY_ID = "_id";
	public static final String ENQUIRY_CUSTOMER_ID = "customer_id";
	public static final String ENQUIRY_CUSOTMER_NAME = "customer_name";
	public static final String ENQUIRY_CUTOMER_EMAIL = "customer_email";
	public static final String ENQUIRY_FIBC_STYLE = "fibc_style";
	public static final String ENQUIRY_QUANTITY = "quantity";
	public static final String ENQUIRY_FIBC_TYPE = "fibc_type";
	public static final String ENQUIRY_LOOP_TYPE = "loop_type";
	public static final String ENQUIRY_DIMENSION = "dimension";
	public static final String ENQUIRY_ATTRIBUTES = "attributes";
	public static final String ENQUIRY_BOTTOM_ATTRIBUTES = "bottom_attributes";
	public static final String ENQUIRY_TIE_ATTRIBUTES = "tie_attributes";
	public static final String ENQUIRY_FIBC_ATTRIBUTES = "fibc_attributes";
	public static final String ENQUIRY_EXTRA_ATTRIBUTES = "extra_attributes";
	public static final String ENQUIRY_FRONT_STYLE = "front_style";
	public static final String ENQUIRY_BOTTOM_STYLE = "bottom_style";
	public static final String ENQUIRY_LOOP_ATTRIBUTES = "loop_attributes";
	public static final String ENQUIRY_DROOL_RULE_NAME = "drool_rule_name";
	public static final String ENQUIRY_TOTAL_WEIGHT = "total_weight";
	public static final String ENQUIRY_BASIC_BAG_WEIGHT = "basic_bag_weight";
	public static final String ENQUIRY_ACTUAL_BAG_WEIGHT = "actual_weight";
	public static final String ENQUIRY_ACCESSORIES_WEIGHT = "accessories_weight";
	public static final String ENQUIRY_TOTAL_COST = "total_cost";
	public static final String ENQUIRY_FIBC_QUOTATION = "fibc_quotation";
	public static final String ENQUIRY_DATE = "enquiry_date";
	public static final String ENQUIRY_COMPONENT = "component";
	public static final String ENQUIRY_ACCESSORIES = "accessories";
	public static final String ENQUIRY_PRINTING = "printing";
	public static final String ENQUIRY_EXTRAS = "extras";
	public static final String ENQUIRY_OTHER_COMPONENT = "other_component";
	public static final String ENQUIRY_UV_GRADE = "uv_grade";
	public static final String ENQUIRY_QUOTATION = "quotation";
	public static final String ENQUIRY_USD = "USD";
	public static final String ENQUIRY_INR = "INR";
	public static final String ENQUIRY_PREVIEW_DETAILS = "fibc_preview_details";

	@Id
	@Field(ENQUIRY_ID)
	private String id;

	@Field(ENQUIRY_CUSTOMER_ID)
	private String cusotmerID;

	@Field(ENQUIRY_CUSOTMER_NAME)
	private String customerName;

	@Field(ENQUIRY_CUTOMER_EMAIL)
	private String customerEmail;

	@Field(ENQUIRY_FIBC_STYLE)
	private String fibcStyle;

	@Field(ENQUIRY_QUANTITY)
	private int quantity;

	@Field(ENQUIRY_FIBC_TYPE)
	private String fibcType;

	@Field(ENQUIRY_LOOP_TYPE)
	private LoopStyleProperties loopType;

	@Field(ENQUIRY_DIMENSION)
	private List<Attributes> dimension;

	@Field(ENQUIRY_ATTRIBUTES)
	private List<Attributes> attributes;

	@Field(ENQUIRY_BOTTOM_ATTRIBUTES)
	private List<Attributes> bottomAttributes;

	@Field(ENQUIRY_TIE_ATTRIBUTES)
	private List<Attributes> tieAttributes;

	@Field(ENQUIRY_FIBC_ATTRIBUTES)
	private List<Attributes> fibcAttributes;

	@Field(ENQUIRY_EXTRA_ATTRIBUTES)
	private List<Attributes> extraAttributes;

	@Field(ENQUIRY_FRONT_STYLE)
	private TopBottomStypeProperties frontStyle;

	@Field(ENQUIRY_BOTTOM_STYLE)
	private TopBottomStypeProperties bottomStyle;

	@Field(ENQUIRY_LOOP_ATTRIBUTES)
	private List<Attributes> loopAttributes;

	@Field(ENQUIRY_DROOL_RULE_NAME)
	private String droolRuleName;

	@Field(ENQUIRY_TOTAL_WEIGHT)
	private double totalWeight;

	@Field(ENQUIRY_BASIC_BAG_WEIGHT)
	private double basicBagWeight;

	@Field(ENQUIRY_ACTUAL_BAG_WEIGHT)
	private double actualBagWeight;

	@Field(ENQUIRY_ACCESSORIES_WEIGHT)
	private double accessoriesWeight;

	@Field(ENQUIRY_TOTAL_COST)
	private double totalCost;

	@Field(ENQUIRY_FIBC_QUOTATION)
	private List<FibcQuotation> fibcQuotation;

	@Field(ENQUIRY_QUOTATION)
	private FibcQuotation quotation = new FibcQuotation();

	@Field(ENQUIRY_DATE)
	private String enquiryDate;

	@Field(ENQUIRY_COMPONENT)
	private FibcComponent selectedComponent;

	@Field(ENQUIRY_ACCESSORIES)
	private FibcComponent selectedAccessory;

	@Field(ENQUIRY_PRINTING)
	private FibcComponent printing;

	@Field(ENQUIRY_EXTRAS)
	private List<FibcComponent> extras;

	@Field(ENQUIRY_OTHER_COMPONENT)
	private List<FibcComponent> otherComponentList;

	@Field(ENQUIRY_UV_GRADE)
	private String uvGrade;

	@Field(ENQUIRY_USD)
	private double usd;

	@Field(ENQUIRY_INR)
	private double inr;

	@Field(ENQUIRY_PREVIEW_DETAILS)
	private FibcPreviewDetails fibcPreviewDetails;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCusotmerID() {
		return cusotmerID;
	}

	public void setCusotmerID(String cusotmerID) {
		this.cusotmerID = cusotmerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getFibcStyle() {
		return fibcStyle;
	}

	public void setFibcStyle(String fibcStyle) {
		this.fibcStyle = fibcStyle;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getFibcType() {
		return fibcType;
	}

	public void setFibcType(String fibcType) {
		this.fibcType = fibcType;
	}

	public LoopStyleProperties getLoopType() {
		return loopType;
	}

	public void setLoopType(LoopStyleProperties loopType) {
		this.loopType = loopType;
	}

	public List<Attributes> getDimension() {
		return dimension;
	}

	public void setDimension(List<Attributes> dimension) {
		this.dimension = dimension;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public List<Attributes> getBottomAttributes() {
		return bottomAttributes;
	}

	public void setBottomAttributes(List<Attributes> bottomAttributes) {
		this.bottomAttributes = bottomAttributes;
	}

	public List<Attributes> getTieAttributes() {
		return tieAttributes;
	}

	public void setTieAttributes(List<Attributes> tieAttributes) {
		this.tieAttributes = tieAttributes;
	}

	public List<Attributes> getFibcAttributes() {
		return fibcAttributes;
	}

	public void setFibcAttributes(List<Attributes> fibcAttributes) {
		this.fibcAttributes = fibcAttributes;
	}

	public List<Attributes> getExtraAttributes() {
		return extraAttributes;
	}

	public void setExtraAttributes(List<Attributes> extraAttributes) {
		this.extraAttributes = extraAttributes;
	}

	public TopBottomStypeProperties getFrontStyle() {
		return frontStyle;
	}

	public void setFrontStyle(TopBottomStypeProperties frontStyle) {
		this.frontStyle = frontStyle;
	}

	public TopBottomStypeProperties getBottomStyle() {
		return bottomStyle;
	}

	public void setBottomStyle(TopBottomStypeProperties bottomStyle) {
		this.bottomStyle = bottomStyle;
	}

	public List<Attributes> getLoopAttributes() {
		return loopAttributes;
	}

	public void setLoopAttributes(List<Attributes> loopAttributes) {
		this.loopAttributes = loopAttributes;
	}

	public String getDroolRuleName() {
		return droolRuleName;
	}

	public void setDroolRuleName(String droolRuleName) {
		this.droolRuleName = droolRuleName;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public double getBasicBagWeight() {
		return basicBagWeight;
	}

	public void setBasicBagWeight(double basicBagWeight) {
		this.basicBagWeight = basicBagWeight;
	}

	public double getActualBagWeight() {
		return actualBagWeight;
	}

	public void setActualBagWeight(double actualBagWeight) {
		this.actualBagWeight = actualBagWeight;
	}

	public double getAccessoriesWeight() {
		return accessoriesWeight;
	}

	public void setAccessoriesWeight(double accessoriesWeight) {
		this.accessoriesWeight = accessoriesWeight;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public List<FibcQuotation> getFibcQuotation() {
		return fibcQuotation;
	}

	public void setFibcQuotation(List<FibcQuotation> fibcQuotation) {
		this.fibcQuotation = fibcQuotation;
	}

	public String getEnquiryDate() {
		return enquiryDate;
	}

	public void setEnquiryDate(String enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	public FibcComponent getSelectedComponent() {
		return selectedComponent;
	}

	public void setSelectedComponent(FibcComponent selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

	public FibcComponent getSelectedAccessory() {
		return selectedAccessory;
	}

	public void setSelectedAccessory(FibcComponent selectedAccessory) {
		this.selectedAccessory = selectedAccessory;
	}

	public FibcComponent getPrinting() {
		return printing;
	}

	public void setPrinting(FibcComponent printing) {
		this.printing = printing;
	}

	public List<FibcComponent> getExtras() {
		return extras;
	}

	public void setExtras(List<FibcComponent> extras) {
		this.extras = extras;
	}

	public List<FibcComponent> getOtherComponentList() {
		return otherComponentList;
	}

	public void setOtherComponentList(List<FibcComponent> otherComponentList) {
		this.otherComponentList = otherComponentList;
	}

	public String getUvGrade() {
		return uvGrade;
	}

	public void setUvGrade(String uvGrade) {
		this.uvGrade = uvGrade;
	}

	public FibcQuotation getQuotation() {
		return quotation;
	}

	public void setQuotation(FibcQuotation quotation) {
		this.quotation = quotation;
	}

	public double getUsd() {
		return usd;
	}

	public void setUsd(double usd) {
		this.usd = usd;
	}

	public double getInr() {
		return inr;
	}

	public void setInr(double inr) {
		this.inr = inr;
	}

	public FibcPreviewDetails getFibcPreviewDetails() {
		return fibcPreviewDetails;
	}

	public void setFibcPreviewDetails(FibcPreviewDetails fibcPreviewDetails) {
		this.fibcPreviewDetails = fibcPreviewDetails;
	}

}