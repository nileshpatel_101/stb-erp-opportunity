package com.divergentsl.erp.opportunity.mongodb.documents;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "fibc_certificate")
public class FibcCertificate implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final String SERIAL_NO = "serial_no";
	private static final String CERTIFICATE_CETEGORY = "certificate_cetegory";
	private static final String FIBC_CERTIFICATE = "certificate";
	private static final String FIBC_SWL = "swl";
	private static final String FIBC_SF = "sf";
	private static final String FIBC_LENGTH = "length";
	private static final String FIBC_WIDTH = "width";
	private static final String MINIMUM_HEIGHT = "minimum_height";
	private static final String MAXIMUM_HEIGHT = "maximum_height";
	private static final String FABRIC_BODY_GSM = "fabric_body_gsm";
	private static final String FABRIC_SIDE_GSM = "fabric_side_gsm";

	private static final String FABRIC_BODY_COATED = "fabric_body_coated";
	private static final String TYPE_OF_BAG = "type_of_bag";
	private static final String DESIGN_OF_BAG = "design_of_bag";
	private static final String TEST_STANDARD = "test_standard";
	private static final String TEST_HOUSE = "test_house";
	private static final String FIBC_CLASS = "fibc_class";
	private static final String ISSUE_DATE = "issue_date";
	private static final String EXPIRY_DATE = "expiry_date";

	@Id
	@Field(SERIAL_NO)
	private String serialNo;

	@Field(FIBC_CERTIFICATE)
	private String certificate;

	@Field(CERTIFICATE_CETEGORY)
	private String certificateCetegory;

	@Field(FIBC_SWL)
	private int swl;

	@Field(FIBC_SF)
	private String sf;

	@Field(FIBC_LENGTH)
	private int length;

	@Field(FIBC_WIDTH)
	private int width;

	@Field(MINIMUM_HEIGHT)
	private int minimumHeight;

	@Field(MAXIMUM_HEIGHT)
	private int maximumHeight;

	@Field(FABRIC_BODY_GSM)
	private String fabricBodyGsm;

	@Field(FABRIC_SIDE_GSM)
	private String fabricSideGsm;

	@Field(FABRIC_BODY_COATED)
	private String fabricBodyCoated;

	@Field(TYPE_OF_BAG)
	private String typeOfBag;

	@Field(DESIGN_OF_BAG)
	private String designOfBag;

	@Field(TEST_STANDARD)
	private String testStandard;

	@Field(TEST_HOUSE)
	private String testHouse;

	@Field(FIBC_CLASS)
	private String fibcClass;

	@Field(ISSUE_DATE)
	private Date issueDate;

	@Field(EXPIRY_DATE)
	private Date expiryDate;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public int getSwl() {
		return swl;
	}

	public void setSwl(int swl) {
		this.swl = swl;
	}

	public String getSf() {
		return sf;
	}

	public void setSf(String sf) {
		this.sf = sf;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public String getTypeOfBag() {
		return typeOfBag;
	}

	public void setTypeOfBag(String typeOfBag) {
		this.typeOfBag = typeOfBag;
	}

	public String getDesignOfBag() {
		return designOfBag;
	}

	public void setDesignOfBag(String designOfBag) {
		this.designOfBag = designOfBag;
	}

	public String getTestStandard() {
		return testStandard;
	}

	public void setTestStandard(String testStandard) {
		this.testStandard = testStandard;
	}

	public String getTestHouse() {
		return testHouse;
	}

	public void setTestHouse(String testHouse) {
		this.testHouse = testHouse;
	}

	public String getFibcClass() {
		return fibcClass;
	}

	public void setFibcClass(String fibcClass) {
		this.fibcClass = fibcClass;
	}

	public String getCertificateCetegory() {
		return certificateCetegory;
	}

	public void setCertificateCetegory(String certificateCetegory) {
		this.certificateCetegory = certificateCetegory;
	}

	public int getMinimumHeight() {
		return minimumHeight;
	}

	public void setMinimumHeight(int minimumHeight) {
		this.minimumHeight = minimumHeight;
	}

	public int getMaximumHeight() {
		return maximumHeight;
	}

	public void setMaximumHeight(int maximumHeight) {
		this.maximumHeight = maximumHeight;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getFabricBodyGsm() {
		return fabricBodyGsm;
	}

	public void setFabricBodyGsm(String fabricBodyGsm) {
		this.fabricBodyGsm = fabricBodyGsm;
	}

	public String getFabricSideGsm() {
		return fabricSideGsm;
	}

	public void setFabricSideGsm(String fabricSideGsm) {
		this.fabricSideGsm = fabricSideGsm;
	}

	public String getFabricBodyCoated() {
		return fabricBodyCoated;
	}

	public void setFabricBodyCoated(String fabricBodyCoated) {
		this.fabricBodyCoated = fabricBodyCoated;
	}

}
