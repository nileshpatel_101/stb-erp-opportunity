package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class Attributes implements Serializable {

	
	private static final long serialVersionUID = 1L;
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_LABEL = "label";
	public static final String ATTRIBUTE_VALUES = "values";
	public static final String ATTRIBUTE_VALUE = "value";
	public static final String ATTRIBUTE_BOOLEAN_VALUE = "boolean_value";
	public static final String ATTRIBUTE_VALIDATION = "validation";
	public static final String ATTRIBUTE_UNIT = "unit";

	@Field(ATTRIBUTE_NAME)
	private String name;

	@Field(ATTRIBUTE_TYPE)
	private String type;

	@Field(ATTRIBUTE_LABEL)
	private String label;

	@Field(ATTRIBUTE_VALUES)
	private List<DropdownValues> values;

	@Field(ATTRIBUTE_VALUE)
	private String value;

	@Field(ATTRIBUTE_BOOLEAN_VALUE)
	private boolean booleanValue;

	@Field(ATTRIBUTE_VALIDATION)
	private FibcValidation validation;

	@Field(ATTRIBUTE_UNIT)
	private String unit;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<DropdownValues> getValues() {
		return values;
	}

	public void setValues(List<DropdownValues> values) {
		this.values = values;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public FibcValidation getValidation() {
		return validation;
	}

	public void setValidation(FibcValidation validation) {
		this.validation = validation;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public boolean isBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

}
