package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcQuotation implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final String FIBC_BODY_COMPONENT_QUOTATION = "fibc_body_componenet_quotation";
	public static final String FIBC_LOOP_QUOTATION = "loop_quotation";
	public static final String FIBC_TOP_STYLE_QUOTATION = "top_style_quotation";
	public static final String FIBC_BOTTOM_STYLE_QUOTATION = "bottom_style_quotation";
	public static final String FIBC_COMPONENT_QUOTATION = "fibc_component_quotation";
	public static final String FIBC_ACCESSORIES_QUOTATION = "fibc_accessories_quotation";
	public static final String FIBC_THREADING_QUOTATION = "fibc_threading_quotation";
	public static final String FIBC_PRINTING_QUOTATION = "fibc_printing_quotation";
	public static final String FIBC_COST_QUOTATION = "fibc_cost_quotation";
	public static final String FIBC_LINER_QUOTATION = "fibc_liner_quotation";

	@Field(FIBC_BODY_COMPONENT_QUOTATION)
	private List<FibcQuotationAttributes> fibcBodyComponentQuotation;

	@Field(FIBC_LOOP_QUOTATION)
	private List<FibcQuotationAttributes> fibcLoopQuotation;

	@Field(FIBC_TOP_STYLE_QUOTATION)
	private List<FibcQuotationAttributes> fibcTopStyleQuotation;

	@Field(FIBC_BOTTOM_STYLE_QUOTATION)
	private List<FibcQuotationAttributes> fibcBottomStyleQuotation;

	@Field(FIBC_COMPONENT_QUOTATION)
	private List<FibcQuotationAttributes> fibcComponentsQuotation;

	@Field(FIBC_ACCESSORIES_QUOTATION)
	private List<FibcQuotationAttributes> fibcAccessoriesQuotation;

	@Field(FIBC_THREADING_QUOTATION)
	private List<FibcQuotationAttributes> fibcThreadingQuotation;

	@Field(FIBC_PRINTING_QUOTATION)
	private FibcPrintingQuotation fibcPrintingQuotation;

	@Field(FIBC_COST_QUOTATION)
	private List<FibcCostQuotation> fibcCostQuotations;

	@Field(FIBC_LINER_QUOTATION)
	private FibcLinerQuotation fibcLinerQuotation;

	public List<FibcQuotationAttributes> getFibcBodyComponentQuotation() {
		return fibcBodyComponentQuotation;
	}

	public void setFibcBodyComponentQuotation(List<FibcQuotationAttributes> fibcBodyComponentQuotation) {
		this.fibcBodyComponentQuotation = fibcBodyComponentQuotation;
	}

	public List<FibcQuotationAttributes> getFibcLoopQuotation() {
		return fibcLoopQuotation;
	}

	public void setFibcLoopQuotation(List<FibcQuotationAttributes> fibcLoopQuotation) {
		this.fibcLoopQuotation = fibcLoopQuotation;
	}

	public List<FibcQuotationAttributes> getFibcTopStyleQuotation() {
		return fibcTopStyleQuotation;
	}

	public void setFibcTopStyleQuotation(List<FibcQuotationAttributes> fibcTopStyleQuotation) {
		this.fibcTopStyleQuotation = fibcTopStyleQuotation;
	}

	public List<FibcQuotationAttributes> getFibcBottomStyleQuotation() {
		return fibcBottomStyleQuotation;
	}

	public void setFibcBottomStyleQuotation(List<FibcQuotationAttributes> fibcBottomStyleQuotation) {
		this.fibcBottomStyleQuotation = fibcBottomStyleQuotation;
	}

	public List<FibcQuotationAttributes> getFibcComponentsQuotation() {
		return fibcComponentsQuotation;
	}

	public void setFibcComponentsQuotation(List<FibcQuotationAttributes> fibcComponentsQuotation) {
		this.fibcComponentsQuotation = fibcComponentsQuotation;
	}

	public List<FibcQuotationAttributes> getFibcAccessoriesQuotation() {
		return fibcAccessoriesQuotation;
	}

	public void setFibcAccessoriesQuotation(List<FibcQuotationAttributes> fibcAccessoriesQuotation) {
		this.fibcAccessoriesQuotation = fibcAccessoriesQuotation;
	}

	public List<FibcQuotationAttributes> getFibcThreadingQuotation() {
		return fibcThreadingQuotation;
	}

	public void setFibcThreadingQuotation(List<FibcQuotationAttributes> fibcThreadingQuotation) {
		this.fibcThreadingQuotation = fibcThreadingQuotation;
	}

	public FibcPrintingQuotation getFibcPrintingQuotation() {
		return fibcPrintingQuotation;
	}

	public void setFibcPrintingQuotation(FibcPrintingQuotation fibcPrintingQuotation) {
		this.fibcPrintingQuotation = fibcPrintingQuotation;
	}

	public List<FibcCostQuotation> getFibcCostQuotations() {
		return fibcCostQuotations;
	}

	public void setFibcCostQuotations(List<FibcCostQuotation> fibcCostQuotations) {
		this.fibcCostQuotations = fibcCostQuotations;
	}

	public FibcLinerQuotation getFibcLinerQuotation() {
		return fibcLinerQuotation;
	}

	public void setFibcLinerQuotation(FibcLinerQuotation fibcLinerQuotation) {
		this.fibcLinerQuotation = fibcLinerQuotation;
	}

}
