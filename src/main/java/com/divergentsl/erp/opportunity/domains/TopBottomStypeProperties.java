package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class TopBottomStypeProperties implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String TB_STYLE_ID = "id";
	public static final String TB_STYLE_TYPE = "type";
	public static final String TB_STYLE_IMAGE_URL = "image_url";
	public static final String TB_STYLE_ATTRIBUTES = "attributes";
	public static final String TB_STYLE_TIE_ATTRIBUTES = "tie_attributes";
	public static final String TB_STYLE_CLOUSERE_ATTRIBUTES = "clousere_attributes";
	public static final String TB_STYLE_FLAP_ATTRIBUTES = "flap_attributes";
	public static final String TB_STYLE_FLAP_TIE_ATTRIBUTES = "flap_tie_attributes";
	public static final String TB_STYLE_ROPE_ATTRIBUTES = "rope_attributes";
	public static final String TB_STYLE_IS_RULE_FIRED = "is_rule_fired";
	public static final String TB_STYLE_IS_SELECTED = "is_selected";

	@Field(TB_STYLE_ID)
	private int id;

	@Field(TB_STYLE_TYPE)
	private String type;

	@Field(TB_STYLE_IMAGE_URL)
	private String imageUrl;

	@Field(TB_STYLE_ATTRIBUTES)
	private List<Attributes> attributes;

	@Field(TB_STYLE_TIE_ATTRIBUTES)
	private List<Attributes> tieAttributes;

	@Field(TB_STYLE_CLOUSERE_ATTRIBUTES)
	private List<Attributes> clousereAttributes;

	@Field(TB_STYLE_FLAP_ATTRIBUTES)
	private List<Attributes> flapAttributes;

	@Field(TB_STYLE_FLAP_TIE_ATTRIBUTES)
	private List<Attributes> flapTieAttributes;

	@Field(TB_STYLE_ROPE_ATTRIBUTES)
	private List<Attributes> ropeAttributes;

	@Field(TB_STYLE_IS_RULE_FIRED)
	private boolean isRuleFired = false;

	@Field(TB_STYLE_IS_SELECTED)
	private boolean selected = false;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public List<Attributes> getTieAttributes() {
		return tieAttributes;
	}

	public void setTieAttributes(List<Attributes> tieAttributes) {
		this.tieAttributes = tieAttributes;
	}

	public List<Attributes> getClousereAttributes() {
		return clousereAttributes;
	}

	public void setClousereAttributes(List<Attributes> clousereAttributes) {
		this.clousereAttributes = clousereAttributes;
	}

	public boolean isRuleFired() {
		return isRuleFired;
	}

	public void setRuleFired(boolean isRuleFired) {
		this.isRuleFired = isRuleFired;
	}

	public List<Attributes> getFlapAttributes() {
		return flapAttributes;
	}

	public void setFlapAttributes(List<Attributes> flapAttributes) {
		this.flapAttributes = flapAttributes;
	}

	public List<Attributes> getFlapTieAttributes() {
		return flapTieAttributes;
	}

	public void setFlapTieAttributes(List<Attributes> flapTieAttributes) {
		this.flapTieAttributes = flapTieAttributes;
	}

	public List<Attributes> getRopeAttributes() {
		return ropeAttributes;
	}

	public void setRopeAttributes(List<Attributes> ropeAttributes) {
		this.ropeAttributes = ropeAttributes;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
