package com.divergentsl.erp.opportunity.domains;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileUploadUtils {

	@Value("${file.url}")
	private String uploadFileUrl;

	public String upload(FileThumbnail fileThumbnail) throws Exception {
		byte[] fileInByte = Base64.decodeBase64(fileThumbnail.getBase64());
		String fileNameUUID = java.util.UUID.randomUUID().toString();
		String extension = getExtension(fileThumbnail.getFilename());
		String fileName = fileNameUUID + "." + extension;
		File file = new File(uploadFileUrl);
		if (!file.exists()) {
			file.mkdirs();
		}
		File newFile = new File(uploadFileUrl + fileName);
		OutputStream outputStream = new FileOutputStream(newFile);
		outputStream.write(fileInByte);
		outputStream.close();
		return fileName;

	}

	private String getExtension(String fileName) {

		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		// else
		// throw new GenricException("Invalid ", 417l);
		return fileName;

		/*
		 * String[] split = fileName.split(".");
		 * System.out.println("file name is :::" + fileName + "  &&" +
		 * "  extension is::" + split.toString()); if (split.length < 2) throw
		 * new GenricException("Invalid ", 417l); return split[1];
		 */
	}
}