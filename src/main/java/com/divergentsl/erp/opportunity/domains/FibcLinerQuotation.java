package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcLinerQuotation implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String PARTICULARS_NAME = "particulars_name";
	public static final String LINER_SHAPE = "liner_shape";
	public static final String LINER_TYPE = "liner_type";
	public static final String LINER_ATTACHMENT_TYPE = "liner_attachment_type";
	public static final String LINER_ATTACHMENT_STYLE = "liner_attachment_style";
	public static final String LINER_COLOR = "liner_color";
	public static final String TOTAL_COST = "total_cost";
	public static final String LINER_SHADE = "liner_shade";

	@Field(PARTICULARS_NAME)
	private String particularName;

	@Field(LINER_SHAPE)
	private String linerShape;

	@Field(LINER_TYPE)
	private String linerType;

	@Field(LINER_ATTACHMENT_TYPE)
	private String linerAttachmentType;

	@Field(LINER_ATTACHMENT_STYLE)
	private String linerAttachmentStyle;

	@Field(LINER_COLOR)
	private String linerColor;

	@Field(LINER_SHADE)
	private String linerShade;

	public String getParticularName() {
		return particularName;
	}

	public void setParticularName(String particularName) {
		this.particularName = particularName;
	}

	public String getLinerShape() {
		return linerShape;
	}

	public void setLinerShape(String linerShape) {
		this.linerShape = linerShape;
	}

	public String getLinerType() {
		return linerType;
	}

	public void setLinerType(String linerType) {
		this.linerType = linerType;
	}

	public String getLinerAttachmentType() {
		return linerAttachmentType;
	}

	public void setLinerAttachmentType(String linerAttachmentType) {
		this.linerAttachmentType = linerAttachmentType;
	}

	public String getLinerAttachmentStyle() {
		return linerAttachmentStyle;
	}

	public void setLinerAttachmentStyle(String linerAttachmentStyle) {
		this.linerAttachmentStyle = linerAttachmentStyle;
	}

	public String getLinerColor() {
		return linerColor;
	}

	public void setLinerColor(String linerColor) {
		this.linerColor = linerColor;
	}

	public String getLinerShade() {
		return linerShade;
	}

	public void setLinerShade(String linerShade) {
		this.linerShade = linerShade;
	}

}
