package com.divergentsl.erp.opportunity.domains;



import org.springframework.data.annotation.Transient;

public class FileThumbnail {

	@Transient
	 private String filesize;

	 @Transient
	 private String filetype;

	 @Transient
	 private String filename;

	 @Transient
	 private String base64;

	 public String getFilesize() {
	  return filesize;
	 }

	 public void setFilesize(String filesize) {
	  this.filesize = filesize;
	 }

	 public String getFiletype() {
	  return filetype;
	 }

	 public void setFiletype(String filetype) {
	  this.filetype = filetype;
	 }

	 public String getFilename() {
	  return filename;
	 }

	 public void setFilename(String filename) {
	  this.filename = filename;
	 }

	 public String getBase64() {
	  return base64;
	 }

	 public void setBase64(String base64) {
	  this.base64 = base64;
	 }
}
