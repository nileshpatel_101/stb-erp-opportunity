package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class LoopStyleProperties implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String LOOP_STYLE_ID = "id";
	public static final String LOOP_STYLE_TYPE = "type";
	public static final String LOOP_STYLE_IMAGE_URL = "image_url";
	public static final String LOOP_STYLE_ATTRIBUTES = "attributes";
	public static final String LOOP_STYLE_EXTRA_LOOP_ATTRIBUTES = "extra_loop_attributes";
	public static final String LOOP_STYLE_IS_RULE_FIRED = "is_rule_fired";
	public static final String LOOP_STYLE_IS_SELECTED = "is_selected";

	@Field(LOOP_STYLE_ID)
	private int id;

	@Field(LOOP_STYLE_TYPE)
	private String type;

	@Field(LOOP_STYLE_IMAGE_URL)
	private String imageUrl;

	@Field(LOOP_STYLE_ATTRIBUTES)
	private List<Attributes> attributes;

	@Field(LOOP_STYLE_EXTRA_LOOP_ATTRIBUTES)
	private List<Attributes> extraLoopAttributes;

	@Field(LOOP_STYLE_IS_RULE_FIRED)
	private boolean isRuleFired = false;

	@Field(LOOP_STYLE_IS_SELECTED)
	private boolean selected = false;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public List<Attributes> getExtraLoopAttributes() {
		return extraLoopAttributes;
	}

	public void setExtraLoopAttributes(List<Attributes> extraLoopAttributes) {
		this.extraLoopAttributes = extraLoopAttributes;
	}

	public boolean isRuleFired() {
		return isRuleFired;
	}

	public void setRuleFired(boolean isRuleFired) {
		this.isRuleFired = isRuleFired;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
