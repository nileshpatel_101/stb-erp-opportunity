package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class DropdownValues implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final String DROPDOWN_LABEL = "label";	
	public static final String DROPDOWN_VALUE = "value";
	
	@Field(DROPDOWN_LABEL)
	private String label;
	
	@Field(DROPDOWN_VALUE)
	private String value;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}	
	
		
}
