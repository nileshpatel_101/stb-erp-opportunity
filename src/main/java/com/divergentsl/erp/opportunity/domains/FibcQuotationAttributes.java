package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcQuotationAttributes implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String FIBC_QUOTATION_PARTICULARS_NAME = "particulars_name";
	public static final String FIBC_QUOTATION_GSM = "gsm";
	public static final String FIBC_QUOTATION_COATED_GSM = "coated_gsm";
	public static final String FIBC_QUOTATION_FAB_SIZE = "fab_size";
	public static final String FIBC_QUOTATION_CUT_SIZE = "cut_size";
	public static final String FIBC_QUOTATION_TOTAL_METER = "total_meter";
	public static final String FIBC_QUOTATION_TOTAL_WEIGHT = "total_weight";
	public static final String FIBC_QUOTATION_MARKER = "marker";
	public static final String FIBC_QUOTATION_TOTAL_COST = "total_cost";
	public static final String FIBC_QUOTATION_QUANTITY = "quantity";
	public static final String FIBC_QUOTATION_IMAGE_URL = "image_url";

	@Field(FIBC_QUOTATION_PARTICULARS_NAME)
	private String particularName;

	@Field(FIBC_QUOTATION_GSM)
	private double gsm;

	@Field(FIBC_QUOTATION_COATED_GSM)
	private double coatedGsm;

	@Field(FIBC_QUOTATION_FAB_SIZE)
	private double fabSize;

	@Field(FIBC_QUOTATION_CUT_SIZE)
	private double cutSize;

	@Field(FIBC_QUOTATION_TOTAL_METER)
	private double totalMeter;

	@Field(FIBC_QUOTATION_TOTAL_WEIGHT)
	private double totalWeight;

	@Field(FIBC_QUOTATION_TOTAL_COST)
	private double totalCost;

	@Field(FIBC_QUOTATION_QUANTITY)
	private int quantity;

	@Field(FIBC_QUOTATION_MARKER)
	private String marker;

	@Field(FIBC_QUOTATION_IMAGE_URL)
	private String imageUrl;
		
	public String getParticularName() {
		return particularName;
	}

	public void setParticularName(String particularName) {
		this.particularName = particularName;
	}

	public double getGsm() {
		return gsm;
	}

	public void setGsm(double gsm) {
		this.gsm = gsm;
	}

	public double getCoatedGsm() {
		return coatedGsm;
	}

	public void setCoatedGsm(double coatedGsm) {
		this.coatedGsm = coatedGsm;
	}

	public double getFabSize() {
		return fabSize;
	}

	public void setFabSize(double fabSize) {
		this.fabSize = fabSize;
	}

	public double getCutSize() {
		return cutSize;
	}

	public void setCutSize(double cutSize) {
		this.cutSize = cutSize;
	}

	public double getTotalMeter() {
		return totalMeter;
	}

	public void setTotalMeter(double totalMeter) {
		this.totalMeter = totalMeter;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMarker() {
		return marker;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
		

}
