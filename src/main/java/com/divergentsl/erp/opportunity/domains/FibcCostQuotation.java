package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcCostQuotation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final String PARTICULAR_NAME = "particular_name";
	public static final String PARTICULAR_RATE_USD = "particular_rate_usd";
	public static final String PARTICULAR_RATE_UNIT = "particular_rate_unit";
	public static final String COST_PER_BAG_USD = "cost_per_bag_usd";
	public static final String COST_PER_BAG_INR = "cost_per_bag_inr";
	public static final String TOTAL_COST = "total_cost";

	@Field(PARTICULAR_NAME)
	private String particularName;

	@Field(PARTICULAR_RATE_USD)
	private double particularRateUSD;

	@Field(PARTICULAR_RATE_UNIT)
	private String particularRateUnit;

	@Field(COST_PER_BAG_USD)
	private double costPerBagUSD;

	@Field(COST_PER_BAG_INR)
	private double costPerBagINR;

	@Field(TOTAL_COST)
	private double totalCost;

	public String getParticularName() {
		return particularName;
	}

	public void setParticularName(String particularName) {
		this.particularName = particularName;
	}

	public double getParticularRateUSD() {
		return particularRateUSD;
	}

	public void setParticularRateUSD(double particularRateUSD) {
		this.particularRateUSD = particularRateUSD;
	}

	public String getParticularRateUnit() {
		return particularRateUnit;
	}

	public void setParticularRateUnit(String particularRateUnit) {
		this.particularRateUnit = particularRateUnit;
	}

	public double getCostPerBagUSD() {
		return costPerBagUSD;
	}

	public void setCostPerBagUSD(double costPerBagUSD) {
		this.costPerBagUSD = costPerBagUSD;
	}

	public double getCostPerBagINR() {
		return costPerBagINR;
	}

	public void setCostPerBagINR(double costPerBagINR) {
		this.costPerBagINR = costPerBagINR;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

}
