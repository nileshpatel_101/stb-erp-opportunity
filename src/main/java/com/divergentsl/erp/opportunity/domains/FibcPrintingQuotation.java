package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcPrintingQuotation implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String PARTICULARS_NAME = "particulars_name";
	public static final String FIBC_PRINTING_DESCRIPTION = "description";
	public static final String FIBC_PRINTING_COLOR_DESCRIPTION = "color_description";
	public static final String FIBC_PRINTING_SIDE_DESCRIPTION = "side_description";
	public static final String FIBC_PRINTING_TOTAL_COST = "total_cost";

	@Field(PARTICULARS_NAME)
	private String particularName;

	@Field(FIBC_PRINTING_DESCRIPTION)
	private String description;

	@Field(FIBC_PRINTING_COLOR_DESCRIPTION)
	private String colorDescription;

	@Field(FIBC_PRINTING_SIDE_DESCRIPTION)
	private String sideDescription;

	@Field(FIBC_PRINTING_TOTAL_COST)
	private double totalCost;

	public String getParticularName() {
		return particularName;
	}

	public void setParticularName(String particularName) {
		this.particularName = particularName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColorDescription() {
		return colorDescription;
	}

	public void setColorDescription(String colorDescription) {
		this.colorDescription = colorDescription;
	}

	public String getSideDescription() {
		return sideDescription;
	}

	public void setSideDescription(String sideDescription) {
		this.sideDescription = sideDescription;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

}
