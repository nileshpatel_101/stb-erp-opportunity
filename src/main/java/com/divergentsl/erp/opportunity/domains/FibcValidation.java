package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;

public class FibcValidation implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String VALIDATION_MIN = "min";
	public static final String VALIDATION_MAX = "max";
	public static final String VALIDATION_REQUIRED = "required";
	public static final String VALIDATION_PATTERN = "pattern";
	public static final String VALIDATION_IF_CONDITION = "if_condition";
	public static final String VALIDATION_FUNCTION = "function";
	public static final String VALIDATION_FUNCTION_ARGUMENTS = "function_arguments";

	@Field(VALIDATION_MIN)
	private int min;

	@Field(VALIDATION_MAX)
	private int max;

	@Field(VALIDATION_REQUIRED)
	private boolean required;

	@Field(VALIDATION_PATTERN)
	private String pattern;

	@Field(VALIDATION_IF_CONDITION)
	private String ifCondition;

	@Field(VALIDATION_FUNCTION)
	private String function;

	@Field(VALIDATION_FUNCTION_ARGUMENTS)
	private String functionArguments;

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getIfCondition() {
		return ifCondition;
	}

	public void setIfCondition(String ifCondition) {
		this.ifCondition = ifCondition;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getFunctionArguments() {
		return functionArguments;
	}

	public void setFunctionArguments(String functionArguments) {
		this.functionArguments = functionArguments;
	}

}
