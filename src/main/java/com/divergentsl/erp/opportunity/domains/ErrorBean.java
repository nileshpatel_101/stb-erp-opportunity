package com.divergentsl.erp.opportunity.domains;



public class ErrorBean {
	private String message;
	private Long code;

	public ErrorBean() {

	}

	public ErrorBean(String message, Long code) {
		super();
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

}
