package com.divergentsl.erp.opportunity.domains;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class FibcComponent implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String COMPONENT_ID = "id";
	public static final String COMPONENT_NAME = "name";
	public static final String COMPONENT_HELP_TEXT = "help_text";
	public static final String COMPONENT_IS_SELECTED = "is_selected";
	public static final String COMPONENT_ATTRIBUTES = "attributes";
	public static final String COMPONENT_QUOTATION = "quotation";
	public static final String COMPONENT_IMAGE_URL = "image_url";

	@Id
	@Field(COMPONENT_ID)
	private String id;

	@Field(COMPONENT_NAME)
	private String name;

	@Field(COMPONENT_HELP_TEXT)
	private String helpText;

	@Field(COMPONENT_IS_SELECTED)
	private boolean isSelected;

	@Field(COMPONENT_ATTRIBUTES)
	private List<Attributes> attributes;

	@Field(COMPONENT_QUOTATION)
	private FibcQuotation quotation;
	
	@Field(COMPONENT_IMAGE_URL)
	private String imageUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public FibcQuotation getQuotation() {
		return quotation;
	}

	public void setQuotation(FibcQuotation quotation) {
		this.quotation = quotation;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}	
}
