package com.divergentsl.erp.opportunity.resources;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.divergentsl.erp.opportunity.constants.UrlConstants;
import com.divergentsl.erp.opportunity.exceptions.OpportunityValidationException;
import com.divergentsl.erp.opportunity.mongodb.documents.CurrencyDetails;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcModel;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcPreviewDetails;
import com.divergentsl.erp.opportunity.services.EmailSenderJobService;
import com.divergentsl.erp.opportunity.services.OpportunityService;


@RestController
@RequestMapping("/fibc")
public class OpportunityResources {

	@Autowired
	private OpportunityService opportunityService;

	@Autowired
	private EmailSenderJobService emailSenderJobService;

	@RequestMapping(value = UrlConstants.SAVE_FIBC_MODEL, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FibcModel> saveFibcModelInformation(@RequestBody FibcModel fibcModel) {
		FibcModel updatedFibcModel;
		try {
			updatedFibcModel = opportunityService.saveFibcModelInformation(fibcModel);
		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(fibcModel);
		}
		return ResponseEntity.status(HttpStatus.OK).body(updatedFibcModel);
	}

	@RequestMapping(value = UrlConstants.GET_ALL_FIBC_MODEL, method = RequestMethod.GET)
	public ResponseEntity<List<FibcModel>> getAllFibcModel() {
		return new ResponseEntity<List<FibcModel>>(opportunityService.getFibcAllModel(), HttpStatus.OK);
	}

	@RequestMapping(value = UrlConstants.SAVE_ENQUIRY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FibcEnquiry> registration(@RequestBody FibcEnquiry fibcEnquiry) {
		try {
			emailSenderJobService.sendMail(opportunityService.saveEnquiry(fibcEnquiry));

		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(fibcEnquiry);
		}
		return ResponseEntity.status(HttpStatus.OK).body(fibcEnquiry);
	}

	@RequestMapping(value = UrlConstants.GET_ENQUIRY_BY_ENQUIRY_ID, method = RequestMethod.GET)
	public ResponseEntity<FibcEnquiry> getEnquiryByEnquiryID(@RequestParam("enquiryId") String enquiryId) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(opportunityService.getEnquiryByEnquiryID(enquiryId));
		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
		}

	}

	@RequestMapping(value = UrlConstants.GET_ALL_ENQUIRY, method = RequestMethod.GET)
	public ResponseEntity<List<FibcEnquiry>> getAllEnquiry() {
		return new ResponseEntity<List<FibcEnquiry>>(opportunityService.getAllEnquiry(), HttpStatus.OK);
	}

	@RequestMapping(value = UrlConstants.GET_TOTAL_WEIGHT_AND_COST_FOR_ENQUIRY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FibcEnquiry> getTotalWeightAndCostForEnquiry(@RequestBody FibcEnquiry fibcEnquiry) {

		if (fibcEnquiry.getEnquiryDate() == null) {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or
																			// whatever
																			// IST
																			// is
																			// supposed
																			// to
																			// be
			fibcEnquiry.setEnquiryDate(formatter.format(new Date()));
		}

		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		kSession.insert(fibcEnquiry);
		// new RuleNameEndsWithAgendaFilter( fibcEnquiry.getDroolRuleName())
		kSession.fireAllRules();
		return new ResponseEntity<>(fibcEnquiry, HttpStatus.OK);
	}

	@RequestMapping(value = UrlConstants.SAVE_FIBC_PREVIEW_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FibcPreviewDetails> saveFibcPreviewDetails(
			@RequestBody FibcPreviewDetails fibcPreviewDetails) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(opportunityService.saveFibcPreviewDetails(fibcPreviewDetails));
		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(fibcPreviewDetails);
		}
	}

	@RequestMapping(value = UrlConstants.GET_FIBC_PREVIEW_DETAILS, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FibcPreviewDetails> getFibcPreviewDetails(@RequestParam("id") String id) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(opportunityService.getFibcPreviewDetails(id));
		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
		}
	}

	@RequestMapping(value = UrlConstants.CHECK_CERTIFICATE_ENQUIRY, method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Boolean> checkCertificateForEnquiry(@RequestBody FibcEnquiry fibcEnquiry) {
		boolean check = opportunityService.checkCertificateForEnquiry(fibcEnquiry);
		if (check) {
			return ResponseEntity.status(HttpStatus.OK).body(check);
		} else
			return ResponseEntity.status(HttpStatus.OK).body(check);
	}

	@RequestMapping(value = UrlConstants.GET_CURRENCY_RATE, method = RequestMethod.GET)
	public ResponseEntity<List<CurrencyDetails>> getCurrencyRate() {
		List<CurrencyDetails> currencyDetails = null;
		try {
			currencyDetails = opportunityService.getCurrencyRate();
			return ResponseEntity.status(HttpStatus.OK).body(currencyDetails);
		} catch (OpportunityValidationException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(currencyDetails);
		}
	}

	@RequestMapping(value = "/read_excel", method = RequestMethod.POST)
	public void readXlsx(@RequestParam(value = "file") MultipartFile multipartFile, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		try {
			byte[] pdfData = opportunityService.uploadXlns(multipartFile);
			// return ResponseEntity.status(HttpStatus.OK).body("success");
		} catch (OpportunityValidationException e) {
			// return
			// ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("failed");
		}
	}
}
