package com.divergentsl.erp.opportunity.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.divergentsl.erp.opportunity.mongodb.documents.CurrencyDetails;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcModel;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcPreviewDetails;

public interface OpportunityService {

	FibcModel saveFibcModelInformation(FibcModel fibcModel);

	List<FibcModel> getFibcAllModel();

	FibcEnquiry saveEnquiry(FibcEnquiry fibcEnquiry);

	List<FibcEnquiry> getAllEnquiry();

	FibcEnquiry getEnquiryByEnquiryID(String enquiryID);

	FibcPreviewDetails saveFibcPreviewDetails(FibcPreviewDetails fibcPreview);

	FibcPreviewDetails getFibcPreviewDetails(String id);

	boolean checkCertificateForEnquiry(FibcEnquiry fibcEnquiry);

	public void saveCrrency(CurrencyDetails currencyDetails);

	List<CurrencyDetails> getCurrencyRate();

	byte[] uploadXlns(MultipartFile multipartFile);

}
