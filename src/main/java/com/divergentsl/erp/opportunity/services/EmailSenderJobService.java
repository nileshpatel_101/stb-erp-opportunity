package com.divergentsl.erp.opportunity.services;

import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;

public interface EmailSenderJobService {
	
	public boolean sendMail(FibcEnquiry fibcEnquiry);
}
