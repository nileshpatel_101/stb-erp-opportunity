package com.divergentsl.erp.opportunity.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;
import com.divergentsl.erp.opportunity.services.EmailSenderJobService;

@Service
public class EmailSenderJobServiceImpl implements EmailSenderJobService {

	protected static final Logger logger = Logger.getLogger(EmailSenderJobServiceImpl.class);

	@Value("${spring.mail.username}")
	private String from;

	@Value("${spring.mail.username}")
	private String applicationEmailAddress;

	@Value("${spring.mail.password}")
	private String smtpPassword;

	@Value("${spring.mail.host}")
	private String host;

	@Value("${spring.mail.port}")
	private String port;

	@Value("${baseurl.web}")
	private String baseUrlWeb;

	@Value("${app.title}")
	private String applicationTitle;

	Transport transport = null;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private VelocityEngine velocityEngine;

	public boolean sendMail(FibcEnquiry fibcEnquiry) {
		try {
			javaMailSender.send(sendEnquiryMail(fibcEnquiry));
		} catch (Exception e1) {
			logger.error(e1.getMessage());
		}
		logger.info("<<<<<<<<<<<<<<<<<<<<< email sending >>>>>>>>>>>>>>>>>>>>>>>>>>");
		return false;
	}

	public String getStringValue(List<Attributes> attributes, String key) {
		for (Attributes object : attributes) {
			if (object.getName().equals(key))
				return object.getValue();
		}
		return null;
	}

	private MimeMessage sendEnquiryMail(FibcEnquiry fibcEnquiry) throws MessagingException {

		Map<String, Object> model = new HashMap<>();
		model.put("enquiryHeadPersonName", "Vandana Rajpoot");
		model.put("customerName", "XYZ");
		model.put("fibcModel", fibcEnquiry.getFibcStyle());
		model.put("enquiryID", fibcEnquiry.getId());
		model.put("quantity", fibcEnquiry.getQuantity());
		model.put("safetyFactor", getStringValue(fibcEnquiry.getFibcAttributes(), "SF"));
		model.put("safeWorkingLoad", getStringValue(fibcEnquiry.getFibcAttributes(), "swl"));
		model.put("bodyFabric", getStringValue(fibcEnquiry.getAttributes(), "gsm") + ", Coated GSM "
				+ getStringValue(fibcEnquiry.getAttributes(), "coated-gsm"));
		
		model.put("dimension",
				getStringValue(fibcEnquiry.getDimension(), "inner-length") + 'x'
						+ getStringValue(fibcEnquiry.getDimension(), "inner-width") + 'x'
						+ getStringValue(fibcEnquiry.getDimension(), "inner-height"));
		model.put("topStyle", fibcEnquiry.getFrontStyle().getType());
		model.put("bottomStyle", fibcEnquiry.getBottomStyle().getType());
		if (fibcEnquiry.getQuotation().getFibcPrintingQuotation() != null) {
			model.put("printing", fibcEnquiry.getQuotation().getFibcPrintingQuotation());
		}
		if (!fibcEnquiry.getQuotation().getFibcAccessoriesQuotation().isEmpty()) {
			model.put("accessories", fibcEnquiry.getQuotation().getFibcAccessoriesQuotation());
		}
		if (!fibcEnquiry.getQuotation().getFibcComponentsQuotation().isEmpty()) {
			model.put("components", fibcEnquiry.getQuotation().getFibcComponentsQuotation());
		}

		MimeMessage mime = this.javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mime, true);
		helper.setFrom(this.applicationTitle + " <" + this.applicationEmailAddress + ">");
		String[] emialReceiver = { "vandana.rajpoot@divergentsl.com"};
		helper.setTo(emialReceiver);
		helper.setSubject("FIBC Enquiry");

		model.put("registerLink", "wwww.gmail.com");
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template/mail/enquiry.vm",
				"UTF-8", model);

		helper.setText(text, true);
		return mime;
	}

}
