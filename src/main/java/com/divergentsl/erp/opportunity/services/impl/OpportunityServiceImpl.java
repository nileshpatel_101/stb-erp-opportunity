package com.divergentsl.erp.opportunity.services.impl;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.mongodb.documents.BeltOrderSpecificationDocument;
import com.divergentsl.erp.opportunity.mongodb.documents.CurrencyDetails;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcCertificate;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcLoomDetailsDocument;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcModel;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcPreviewDetails;
import com.divergentsl.erp.opportunity.repository.BeltOrderSpecificationRepository;
import com.divergentsl.erp.opportunity.repository.CurrencyRepository;
import com.divergentsl.erp.opportunity.repository.FibcCertificateRepository;
import com.divergentsl.erp.opportunity.repository.FibcEnquiryRepository;
import com.divergentsl.erp.opportunity.repository.FibcLoomDetailsRepository;
import com.divergentsl.erp.opportunity.repository.FibcModelRepository;
import com.divergentsl.erp.opportunity.repository.FibcPreviewDetailsRepository;
import com.divergentsl.erp.opportunity.services.OpportunityService;
import com.jaspersoft.mongodb.connection.MongoDbConnection;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class OpportunityServiceImpl implements OpportunityService {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private FibcModelRepository fibcModelRepository;

	@Autowired
	private FibcEnquiryRepository fibcEnquiryRepository;

	@Autowired
	private FibcPreviewDetailsRepository fibcPreviewDetailsRepository;

	@Autowired
	private FibcCertificateRepository fibcCertificateRepository;

	@Autowired
	private CurrencyRepository currencyRepository;

	@Autowired
	private FibcLoomDetailsRepository fibcLoomDetailsRepository;

	@Autowired
	private BeltOrderSpecificationRepository beltOrderSpecificationRepository;

	@Value("${file.url}")
	private String uploadFileUrl;

	@Override
	public FibcModel saveFibcModelInformation(FibcModel fibcModel) {
		return fibcModelRepository.save(fibcModel);
	}

	@Override
	public List<FibcModel> getFibcAllModel() {
		return fibcModelRepository.findAll();
	}

	@Override
	public FibcEnquiry saveEnquiry(FibcEnquiry fibcEnquiry) {
		return fibcEnquiryRepository.save(fibcEnquiry);
	}

	@Override
	public List<FibcEnquiry> getAllEnquiry() {

		Aggregation aggregation = Aggregation.newAggregation(FibcEnquiry.class,
				Aggregation.sort(Sort.Direction.DESC, FibcEnquiry.ENQUIRY_DATE));
		AggregationResults<FibcEnquiry> aggregationResults = mongoTemplate.aggregate(aggregation, "fibc_enquiry",
				FibcEnquiry.class);

		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(aggregationResults.getMappedResults());
		System.out.println("dataSource");
		System.out.println(dataSource.getRecordCount());
		
		try {
			System.out.println("uploadFileUrl"+uploadFileUrl);
			JasperReport jasperReport = JasperCompileManager.compileReport(uploadFileUrl + "enquiry-report.jrxml");
			Map parameters = new HashMap();
			parameters.put("customerName", "List of Contacts");

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
			JasperExportManager.exportReportToPdfFile(jasperPrint, uploadFileUrl + "enquiry-report" + ".pdf");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return aggregationResults.getMappedResults();
	}

	@Override
	public FibcEnquiry getEnquiryByEnquiryID(String enquiryID) {
		return fibcEnquiryRepository.findOne(enquiryID);
	}

	@Override
	public FibcPreviewDetails saveFibcPreviewDetails(FibcPreviewDetails fibcPreview) {
		return fibcPreviewDetailsRepository.save(fibcPreview);
	}

	@Override
	public FibcPreviewDetails getFibcPreviewDetails(String id) {
		return fibcPreviewDetailsRepository.findOne(id);
	}

	@Override
	public boolean checkCertificateForEnquiry(FibcEnquiry fibcEnquiry) {
		boolean check = false;
		List<Attributes> list = fibcEnquiry.getAttributes();
		String gsm = list.get(0).getValue();
		String coatedGsm = list.get(3).getValue();

		List<Attributes> list1 = fibcEnquiry.getDimension();
		int length = Integer.parseInt(list1.get(3).getValue());
		int heigth = Integer.parseInt(list1.get(4).getValue());
		int width = Integer.parseInt(list1.get(5).getValue());
		String fibcStyle = fibcEnquiry.getFibcStyle();

		List<Attributes> list2 = fibcEnquiry.getFibcAttributes();
		int swl = Integer.parseInt(list2.get(2).getValue());
		String sf = list2.get(1).getValue();

		List<FibcCertificate> listfibcCertificate = fibcCertificateRepository.findAll();
		Iterator<FibcCertificate> iterator = listfibcCertificate.iterator();
		while (iterator.hasNext()) {
			FibcCertificate fibcCertificate = iterator.next();

			if (fibcCertificate.getFabricBodyGsm().equals(gsm)
					&& fibcCertificate.getFabricBodyCoated().equals(coatedGsm) && fibcCertificate.getLength() == length
					&& fibcCertificate.getWidth() == width && fibcCertificate.getMinimumHeight() == heigth
					&& fibcCertificate.getDesignOfBag().equals(fibcStyle) && fibcCertificate.getSwl() == swl
					&& fibcCertificate.getSf().equals(sf)) {
				check = true;
			}
		}
		return check;
	}

	@Override
	public void saveCrrency(CurrencyDetails currencyDetails) {

		currencyRepository.deleteAll();
		currencyRepository.save(currencyDetails);
	}

	@Override
	public List<CurrencyDetails> getCurrencyRate() {

		return currencyRepository.findAll();
	}

	
	@Override
	public byte[] uploadXlns(MultipartFile multipartFile) {
		// String filename = UUID.randomUUID().toString() + ".xlsx";

		List<FibcLoomDetailsDocument> fibcLoomDetailsDocumentList = new ArrayList<>();
		List<BeltOrderSpecificationDocument> beltOrderSpecificationDocumentList = new ArrayList<>();
		try {
			// String uploadedFileName = multipartFile.getOriginalFilename();
			// String extension = uploadedFileName.split("\\.")[1];

			// Workbook workbook = new XSSFWorkbook(excelFile);
			Workbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = datatypeSheet.iterator();
			FibcLoomDetailsDocument fibcLoomDetailsDocument = new FibcLoomDetailsDocument();
			BeltOrderSpecificationDocument beltOrderSpecificationDocument = new BeltOrderSpecificationDocument();

			String customerName = null;
			String fileName = null;
			String fileReceivedDate = null;
			String requestDispatchDate = null;
			String sheetType = null;

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					Object cellvalue = getCellValue(cell);

					if (cellvalue != null) {
						if ((cellvalue.toString()).equalsIgnoreCase("Customer Name :")) {
							cell = cellIterator.next();
							customerName = (String) getCellValue(cell);
						} else if ((cellvalue.toString()).equalsIgnoreCase("FILE NO-")) {
							cell = cellIterator.next();
							fileName = (String) getCellValue(cell);
						} else if ((cellvalue.toString()).equalsIgnoreCase("File Received Date :")) {
							cell = cellIterator.next();
							fileReceivedDate = (String) getCellValue(cell);
						} else if ((cellvalue.toString()).equalsIgnoreCase("Req. Dispatch Date (Mkt.)")) {
							cell = cellIterator.next();
							requestDispatchDate = (String) getCellValue(cell);
						} else if ((cellvalue.toString()).equalsIgnoreCase("Body +Side Fabric")
								|| (cellvalue.toString()).equalsIgnoreCase("Spout Fabric")
								|| (cellvalue.toString()).equalsIgnoreCase("Top/Skirt- Fabric")
								|| (cellvalue.toString()).equalsIgnoreCase("Webbing :")) {
							sheetType = (String) getCellValue(cell);
							row = rowIterator.next();
						} else if ((cellvalue.toString()).equalsIgnoreCase("Order No.")) {
							row = rowIterator.next();
							cellIterator = row.cellIterator();
						} else {
							switch (cell.getColumnIndex()) {
							case 0:
								// order number
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setOrderNumber(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setOrderNumber(getCellValue(cell).toString());
								}

								break;
							case 1:
								// bag size
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setBagSize(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setBagSize(getCellValue(cell).toString());
								}

								break;
							case 2:
								// type of bag
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setTypeOfBag(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setTypeOfBag(getCellValue(cell).toString());
								}
								break;
							case 3:
								// order quantity
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setOrderQuantity(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setOrderQuantity(getCellValue(cell).toString());
								}

								break;
							case 4:
								// fabric size
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setBeltGrm(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setFabricWidth(getCellValue(cell).toString());
								}
								break;
							case 5:
								// gsm
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setBeltColor(getCellValue(cell).toString());
								} else {
									fibcLoomDetailsDocument.setRunningMeter(getCellValue(cell).toString());
									fibcLoomDetailsDocument.setFabricGsm(getCellValue(cell).toString());
								}
								break;
							case 6:
								// fabric color
								if (sheetType.equalsIgnoreCase("Webbing :")) {
									beltOrderSpecificationDocument.setOrderQuantity(getCellValue(cell).toString());
									beltOrderSpecificationDocument.setCustomerName(customerName);
									beltOrderSpecificationDocument.setSheetType(sheetType);
									// beltOrderSpecificationDocumentList.add(beltOrderSpecificationDocument);
									System.out.println("beltOrderSpecificationDocument.getOrderNumber()"
											+ beltOrderSpecificationDocument.getOrderNumber());

									if (beltOrderSpecificationDocument.getOrderNumber() != null) {
										beltOrderSpecificationRepository.save(beltOrderSpecificationDocument);
									}

									beltOrderSpecificationDocument = new BeltOrderSpecificationDocument();
								} else {
									fibcLoomDetailsDocument.setFabricColor(getCellValue(cell).toString());
								}
								break;
							case 7:
								// quantity of fabric
								if (!sheetType.equalsIgnoreCase("Webbing :")) {
									fibcLoomDetailsDocument.setQuantityOfFabric(getCellValue(cell).toString());
									fibcLoomDetailsDocument.setCustomerName(customerName);
									fibcLoomDetailsDocument.setSheetType(sheetType);

									if (fibcLoomDetailsDocument.getOrderNumber() != null) {
										fibcLoomDetailsDocumentList.add(fibcLoomDetailsDocument);
									}
									fibcLoomDetailsDocument = new FibcLoomDetailsDocument();

								}
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		MongoDbConnection mongConnection = null;
		try {
			String mongoURI = "mongodb://localhost:27017/stb-new";
			mongConnection = new MongoDbConnection(mongoURI, null, null);
		} catch (Exception e) {

		}

		List<InputStream> temp = new ArrayList<InputStream>();

		try {

			for (FibcLoomDetailsDocument object : fibcLoomDetailsDocumentList) {
				if (object.getOrderNumber() != null) {
					fibcLoomDetailsRepository.deleteAll();
					fibcLoomDetailsRepository.save(object);

					JasperReport jasperReport = JasperCompileManager
							.compileReport(uploadFileUrl + "loom_section.jrxml");
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), mongConnection);
					JasperExportManager.exportReportToPdfFile(jasperPrint,
							uploadFileUrl + "Report" + fibcLoomDetailsDocumentList.indexOf(object) + ".pdf");

					if (object.getSheetType().equalsIgnoreCase("Body +Side Fabric")) {
						PDDocument document = PDDocument.load(new File(
								uploadFileUrl + "Report" + fibcLoomDetailsDocumentList.indexOf(object) + ".pdf"));
						PDPage pdPage = document.getPages().get(0);

						PDPageContentStream contentStream = new PDPageContentStream(document, pdPage, true, true, true);
						String bagSize = object.getBagSize();
						String kept = bagSize.substring(0, bagSize.indexOf("X"));
						Integer width = Integer.parseInt(kept);

						String fabricGsm = object.getFabricGsm();
						String[] tokens = fabricGsm.split("\\+");

						System.out.println("fabricGsm");
						System.out.println(fabricGsm);

						if ((object.getTypeOfBag().toLowerCase()).contains("circullar")) {
							circullarFabricDrawing(contentStream, width - 44);
						} else {

							if (tokens.length > 1) {
								if ((isNumeric(tokens[0]) && isNumeric(tokens[1]))) {
									System.out.println("coated");
									coatedFabricDrawing(contentStream, width);
								}
							} else {
								System.out.println("uncoated");
								unCoatedFabricDrawing(contentStream, width);
							}

						}
						contentStream.close();
						document.save(new File(
								uploadFileUrl + "Report" + fibcLoomDetailsDocumentList.indexOf(object) + ".pdf"));
						//
						document.close();
					}

					temp.add(new FileInputStream(
							new File(uploadFileUrl + "Report" + fibcLoomDetailsDocumentList.indexOf(object) + ".pdf")));
				}

			}

			JasperReport jasperReport2 = JasperCompileManager
					.compileReport(uploadFileUrl + "belt_order_specification.jrxml");
			JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport2, new HashMap(), mongConnection); //
			JasperExportManager.exportReportToPdfFile(jasperPrint2,
					uploadFileUrl + "belt_order_specification" + ".pdf");
			temp.add(new FileInputStream(new File(uploadFileUrl + "belt_order_specification" + ".pdf")));

			OutputStream out = new FileOutputStream(new File(uploadFileUrl + "result.pdf"));
			doMerge(temp, out);

		} catch (Exception e) {
			e.printStackTrace();
		}
		beltOrderSpecificationRepository.deleteAll();
		return null;

	}

	public static Object getCellValue(Cell cell) {
		Object cellvalue = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			cellvalue = cell.getStringCellValue();
			System.out.print(cell.getStringCellValue() + "\t");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			cellvalue = cell.getNumericCellValue();
			System.out.print(cell.getNumericCellValue() + "\t");
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			cellvalue = cell.getBooleanCellValue();
			System.out.print(cell.getBooleanCellValue() + "\t");
			break;
		default:
		}
		return cellvalue;
	}

	@SuppressWarnings("deprecation")
	static void coatedFabricDrawing(PDPageContentStream contentStream, Integer width) {
		try {

			// contentStream.setLineDashPattern(new float[] { 3, 1 }, 0);
			// first half horizontal line
			contentStream.drawLine(140, 120, 280, 120);

			PDFont font = PDType1Font.HELVETICA_BOLD;
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(290, 120);
			contentStream.drawString(width + 1 + 4 * 2 + 1.5 * 2 + " cm");
			contentStream.endText();

			// second half horizontal line
			contentStream.drawLine(350, 120, 490, 120);

			// middle full horizontal line
			contentStream.drawLine(140, 100, 490, 100);

			// first vertical line
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(110, 90);
			contentStream.drawString("1.5 cm");
			contentStream.endText();

			// second vertical line

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(165, 90);
			contentStream.drawString("4 cm");
			contentStream.endText();

			// third vertical line

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(440, 90);
			contentStream.drawString("4 cm");
			contentStream.endText();

			// fourth vertical line

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(480, 90);
			contentStream.drawString("1.5 cm");
			contentStream.endText();

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(260, 90);
			contentStream.drawString(width + "+1 = " + (width + 1) + " cm");
			contentStream.endText();

			contentStream.drawLine(200, 80, 430, 80);

			contentStream.setStrokingColor(Color.red);
			contentStream.setLineWidth(2f);
			contentStream.drawLine(160, 90, 160, 110);
			contentStream.drawLine(200, 90, 200, 110);
			contentStream.drawLine(430, 90, 430, 110);
			contentStream.drawLine(470, 90, 470, 110);

			contentStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	static void unCoatedFabricDrawing(PDPageContentStream contentStream, Integer width) {
		try {

			// first half horizontal line
			contentStream.drawLine(140, 120, 280, 120);

			PDFont font = PDType1Font.HELVETICA_BOLD;
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(290, 120);
			contentStream.drawString(width + 3.5 * 2 + " cm");
			contentStream.endText();

			// second half horizontal line
			contentStream.drawLine(350, 120, 490, 120);

			// middle full horizontal line
			contentStream.drawLine(140, 100, 490, 100);

			// first vertical line

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(120, 90);
			contentStream.drawString("3.5 cm");
			contentStream.endText();

			// second vertical line

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(470, 90);
			contentStream.drawString("3.5 cm");
			contentStream.endText();

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(260, 90);
			contentStream.drawString(width + "+1 = " + (width + 1) + " cm");
			contentStream.endText();

			contentStream.drawLine(170, 80, 460, 80);

			contentStream.setStrokingColor(Color.red);
			contentStream.setLineWidth(2f);
			contentStream.drawLine(170, 90, 170, 110);
			contentStream.drawLine(460, 90, 460, 100);

			contentStream.close();
		} catch (Exception e) {

		}

	}

	@SuppressWarnings("deprecation")
	static void circullarFabricDrawing(PDPageContentStream contentStream, Integer width) {
		try {

			// move stuff a bit up and right

			contentStream.setStrokingColorSpace(PDDeviceRGB.INSTANCE);
			// contentStream.setStrokingColor(Color.red);
			contentStream.setLineWidth(1f);
			contentStream.moveTo(300, 80);
			contentStream.addBezier32(0, 100, 300, 120);
			contentStream.addBezier32(600, 100, 300, 80);

			drawText(contentStream, "12 cm", 80, 180, 140);
			drawText(contentStream, "10 cm", 80, 197, 145);
			drawText(contentStream, "10 cm", 80, 275, 150);
			drawText(contentStream, "12 cm", 80, 290, 150);
			drawText(contentStream, "12 cm", 80, 310, 150);
			drawText(contentStream, "10 cm", 80, 325, 150);
			drawText(contentStream, "10 cm", 80, 405, 145);
			drawText(contentStream, "12 cm", 80, 420, 140);

			contentStream.drawLine(190, 105, 190, 115);
			contentStream.drawLine(202, 103, 202, 117);
			contentStream.drawLine(270, 110, 270, 130);
			contentStream.drawLine(280, 110, 280, 130);

			contentStream.drawLine(320, 110, 320, 130);
			contentStream.drawLine(330, 110, 330, 130);
			contentStream.drawLine(398, 103, 398, 117);
			contentStream.drawLine(410, 105, 410, 115);

			// bottom

			contentStream.drawLine(190, 85, 190, 95);
			contentStream.drawLine(202, 83, 202, 97);
			contentStream.drawLine(270, 70, 270, 90);
			contentStream.drawLine(280, 70, 280, 90);

			contentStream.drawLine(320, 70, 320, 90);
			contentStream.drawLine(330, 70, 330, 90);
			contentStream.drawLine(398, 83, 398, 97);
			contentStream.drawLine(410, 85, 410, 95);

			drawText(contentStream, "12 cm", 80, 177, 90);
			drawText(contentStream, "10 cm", 80, 194, 85);
			drawText(contentStream, "10 cm", 80, 272, 75);
			drawText(contentStream, "12 cm", 80, 287, 75);
			drawText(contentStream, "12 cm", 80, 307, 75);
			drawText(contentStream, "10 cm", 80, 322, 75);
			drawText(contentStream, "10 cm", 80, 402, 85);
			drawText(contentStream, "12 cm", 80, 417, 90);

			drawTextWithoutRotation(contentStream, width + " cm", 217, 120);
			drawTextWithoutRotation(contentStream, width + " cm", 350, 120);
			drawTextWithoutRotation(contentStream, width + " cm", 217, 75);
			drawTextWithoutRotation(contentStream, width + " cm", 350, 75);

			contentStream.setStrokingColor(Color.blue);
			contentStream.setLineWidth(3f);
			contentStream.drawLine(190, 110, 202, 110);
			contentStream.drawLine(270, 118, 280, 118);
			contentStream.drawLine(320, 118, 330, 118);
			contentStream.drawLine(398, 110, 410, 110);

			contentStream.drawLine(190, 90, 202, 90);
			contentStream.drawLine(270, 83, 280, 83);
			contentStream.drawLine(320, 83, 330, 83);
			contentStream.drawLine(398, 90, 410, 90);

			contentStream.setStrokingColor(Color.red);
			contentStream.setLineWidth(2f);
			contentStream.drawLine(300, 105, 300, 135);
			contentStream.drawLine(300, 65, 300, 95);
			contentStream.drawLine(150, 100, 190, 100);
			contentStream.drawLine(410, 100, 450, 100);

			// contentStream.stroke();

			contentStream.close();

		} catch (Exception e) {

		}
	}

	@SuppressWarnings("deprecation")
	public static void drawTextWithoutRotation(PDPageContentStream contentStream, String drawString, float x, float y)
			throws IOException {
		contentStream.setNonStrokingColor(0, 0, 0);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, 10);
		contentStream.moveTextPositionByAmount(x, y);
		contentStream.drawString(drawString);
		contentStream.endText();
	}

	@SuppressWarnings("deprecation")
	public static void drawText(PDPageContentStream contentStream, String drawString, double rotationAngle, double x,
			double y) throws IOException {
		PDFont font = PDType1Font.HELVETICA;
		contentStream.beginText();
		contentStream.setFont(font, 8);
		contentStream.setTextRotation(rotationAngle, x, y);
		contentStream.drawString(drawString);
		contentStream.endText();
	}

	public static void doMerge(List<InputStream> list, OutputStream outputStream)
			throws DocumentException, IOException {
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, outputStream);
		document.open();
		PdfContentByte cb = writer.getDirectContent();

		for (InputStream in : list) {
			PdfReader reader = new PdfReader(in);
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				document.newPage();
				// import the page from source pdf
				PdfImportedPage page = writer.getImportedPage(reader, i);
				// add the page to the destination pdf
				cb.addTemplate(page, 0, 0);
			}
		}

		outputStream.flush();
		document.close();
		outputStream.close();
	}

	@SuppressWarnings("deprecation")
	/*
	 * public void temp() { try {
	 * 
	 * JasperReport jasperReport1 =
	 * JasperCompileManager.compileReport(uploadFileUrl + "loom_section.jrxml");
	 * JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1,
	 * new HashMap(), mongConnection); //
	 * JasperExportManager.exportReportToPdfFile(jasperPrint1, // uploadFileUrl
	 * + "loom_section" + ".pdf");
	 * 
	 * System.out.println("Report Generated");
	 * 
	 * JasperReport jasperReport2 = JasperCompileManager
	 * .compileReport(uploadFileUrl + "belt_order_specification.jrxml");
	 * JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport2,
	 * new HashMap(), mongConnection); //
	 * JasperExportManager.exportReportToPdfFile(jasperPrint2, // uploadFileUrl
	 * + "belt_order_specification" + ".pdf");
	 * 
	 * System.out.println("Report Generated");
	 * 
	 * List pages = jasperPrint2.getPages(); for (int j = 0; j < pages.size();
	 * j++) { JRPrintPage object = (JRPrintPage) pages.get(j);
	 * jasperPrint1.addPage(object); }
	 * 
	 * exportReportToPdf = JasperExportManager.exportReportToPdf(jasperPrint1);
	 * JasperExportManager.exportReportToPdfFile(jasperPrint1, uploadFileUrl +
	 * "planning-details" + ".pdf");
	 * 
	 * // System.setProperty("java.awt.headless", "false"); // // Thread thread
	 * = new Thread(new Runnable() { // @Override // public void run() { //
	 * JasperViewer jasperViewer = new JasperViewer(jasperPrint1); //
	 * jasperViewer.setVisible(true); // } // }); // thread.start();
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */

	void appendContentToExistingPDF() throws InvalidPasswordException, IOException {
		PDDocument document = PDDocument.load(new File(uploadFileUrl + "planning-details" + ".pdf"));
		// List<?> pages = (List<?>) document.getPages();
		List<PDPage> pages = (List<PDPage>) document.getDocumentCatalog().getPages();

		for (Object object : pages) {
			PDPage page = pages.get(pages.indexOf(object));
			PDPageContentStream contentStream = new PDPageContentStream(document, page);

			// contentStream.setLineDashPattern(new float[] { 3, 1 }, 0);
			// first half horizontal line
			contentStream.drawLine(140, 690, 300, 690);

			PDFont font = PDType1Font.HELVETICA_BOLD;
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(310, 690);
			contentStream.drawString("127");
			contentStream.endText();

			// second half horizontal line
			contentStream.drawLine(340, 690, 490, 690);

			// middle full horizontal line
			contentStream.drawLine(140, 670, 490, 670);

			// first vertical line
			contentStream.drawLine(160, 660, 160, 680);
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(110, 660);
			contentStream.drawString("1.5 cm");
			contentStream.endText();

			// second vertical line
			contentStream.drawLine(200, 660, 200, 680);
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(165, 660);
			contentStream.drawString("4 cm");
			contentStream.endText();

			// third vertical line
			contentStream.drawLine(430, 660, 430, 680);
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(440, 660);
			contentStream.drawString("4 cm");
			contentStream.endText();

			// fourth vertical line
			contentStream.drawLine(470, 660, 470, 680);
			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(480, 660);
			contentStream.drawString("1.5 cm");
			contentStream.endText();

			contentStream.setNonStrokingColor(0, 0, 0); // black text
			contentStream.beginText();
			contentStream.setFont(font, 12);
			contentStream.moveTextPositionByAmount(260, 660);
			contentStream.drawString("91+1 = 92 cm");
			contentStream.endText();

			contentStream.drawLine(200, 650, 430, 650);

			contentStream.close();

			document.save(new File("/home/hp/Projects/STB/tirupati/withdrawing.pdf"));
			document.close();

		}

	}

	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional
												// '-' and decimal.
	}

}
