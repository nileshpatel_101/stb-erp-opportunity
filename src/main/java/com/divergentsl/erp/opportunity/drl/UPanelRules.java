package com.divergentsl.erp.opportunity.drl;

import java.util.ArrayList;
import java.util.List;

import com.divergentsl.erp.opportunity.domains.Attributes;
import com.divergentsl.erp.opportunity.domains.FibcCostQuotation;
import com.divergentsl.erp.opportunity.domains.FibcLinerQuotation;
import com.divergentsl.erp.opportunity.domains.FibcPrintingQuotation;
import com.divergentsl.erp.opportunity.domains.FibcQuotationAttributes;
import com.divergentsl.erp.opportunity.mongodb.documents.FibcEnquiry;

public class UPanelRules {

	private UPanelRules(){
		
	}
	public static void fibcComponentsQuotation(FibcEnquiry fibcEnquiry, String particularName, double totalWeight,
			double gsm, double coatedGsm, double totalMeter, String marker, double fabSize, double cutSize) {
		if (fibcEnquiry.getQuotation().getFibcComponentsQuotation() == null) {
			List<FibcQuotationAttributes> fibcPriceQuotationList = new ArrayList<>();
			FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
					cutSize, totalMeter, totalWeight, marker, 0, 0, "");

			fibcPriceQuotationList.add(fibcQuotation);
			fibcEnquiry.getQuotation().setFibcComponentsQuotation(fibcPriceQuotationList);
		} else {
			FibcQuotationAttributes fibcQuotation = UPanelRules
					.getQuotationObject(fibcEnquiry.getQuotation().getFibcComponentsQuotation(), particularName);
			if (fibcQuotation == null) {
				List<FibcQuotationAttributes> fibcQuotationList = fibcEnquiry.getQuotation()
						.getFibcComponentsQuotation();
				fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize, cutSize, totalMeter,
						totalWeight, marker, 0, 0, "");
				fibcQuotationList.add(fibcQuotation);
			} else {
				fibcQuotation.setParticularName(particularName);
				fibcQuotation.setGsm(gsm);
				fibcQuotation.setCoatedGsm(coatedGsm);
				fibcQuotation.setTotalWeight(totalWeight);
				fibcQuotation.setTotalMeter(totalMeter);
				fibcQuotation.setMarker(marker);
				fibcQuotation.setFabSize(fabSize);
				fibcQuotation.setCutSize(cutSize);
			}
		}
	}

	public static double stringToDouble(String s) {
		try {
			return Double.parseDouble(s);
		} catch (Exception e) {
			return -1;
		}
	}

	public static double getValue(List<Attributes> attributes, String key) {
		if (attributes != null) {
			for (Attributes object : attributes) {
				if (object.getName().equals(key))
					return Double.parseDouble(object.getValue());
			}
		}
		return 0;
	}

	public static boolean setValue(List<Attributes> attributes, String key, String value) {
		for (Attributes object : attributes) {
			if (object.getName().equals(key)) {
				object.setValue(value);
				return true;
			}
		}
		return false;
	}

	public static boolean setUvGrade(FibcEnquiry fibcEnquiry, String value) {
		fibcEnquiry.setUvGrade(value);
		return false;
	}

	public static String getStringValue(List<Attributes> attributes, String key) {
		for (Attributes object : attributes) {
			if (object.getName().equals(key))
				return object.getValue();
		}
		return null;
	}

	public static boolean getBooleanValue(List<Attributes> attributes, String key) {
		for (Attributes object : attributes) {
			if (object.getName().equals(key))
				if (object.getValue() != null) {
					return Boolean.valueOf(object.getValue());
				} else if (object.isBooleanValue()) {
					return Boolean.valueOf(object.isBooleanValue());
				}
		}
		return false;
	}

	public static FibcQuotationAttributes getQuotationObject(List<FibcQuotationAttributes> priceQuotations,
			String key) {
		for (FibcQuotationAttributes object : priceQuotations) {
			if (object.getParticularName().equals(key))
				return object;
		}
		return null;
	}

	public static FibcQuotationAttributes setQuotationAttributes(String particularName, double gsm, double coatedGsm,
			double fabSize, double cutSize, double totalMeter, double totalWeight, String marker, double quantity,
			double totalCost, String imageUrl) {
		FibcQuotationAttributes fibcQuotation = new FibcQuotationAttributes();
		fibcQuotation.setParticularName(particularName);
		fibcQuotation.setGsm(gsm);
		fibcQuotation.setCoatedGsm(coatedGsm);
		fibcQuotation.setFabSize(fabSize);
		fibcQuotation.setCutSize(cutSize);
		fibcQuotation.setMarker(marker);
		fibcQuotation.setTotalMeter(totalMeter);
		fibcQuotation.setTotalWeight(totalWeight);
		fibcQuotation.setQuantity((int) quantity);
		fibcQuotation.setTotalCost(totalCost);
		fibcQuotation.setImageUrl(imageUrl);
		return fibcQuotation;
	}

	public static void fibcQuotation(FibcEnquiry fibcEnquiry, String status, String particularName, double gsm,
			double coatedGsm, double fabSize, double cutSize, double totalMeter, double totalWeight, String marker) {
		if (status.equals("BODY-COMPONENT")) {
			if (fibcEnquiry.getQuotation().getFibcBodyComponentQuotation() == null) {
				List<FibcQuotationAttributes> fibcPriceQuotationList = new ArrayList<>();
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcPriceQuotationList.add(fibcQuotation);
				fibcEnquiry.getQuotation().setFibcBodyComponentQuotation(fibcPriceQuotationList);
			} else {
				FibcQuotationAttributes fibcQuotation = UPanelRules
						.getQuotationObject(fibcEnquiry.getQuotation().getFibcBodyComponentQuotation(), particularName);
				if (fibcQuotation == null) {
					List<FibcQuotationAttributes> fibcQuotationList = fibcEnquiry.getQuotation()
							.getFibcBodyComponentQuotation();
					fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize, cutSize, totalMeter,
							totalWeight, marker, 0, 0, "");
					fibcQuotationList.add(fibcQuotation);
				} else {
					fibcQuotation.setParticularName(particularName);
					fibcQuotation.setGsm(gsm);
					fibcQuotation.setCoatedGsm(coatedGsm);
					fibcQuotation.setFabSize(fabSize);
					fibcQuotation.setCutSize(cutSize);
					fibcQuotation.setTotalMeter(totalMeter);
					fibcQuotation.setTotalWeight(totalWeight);
				}
			}
		}

		if (status.equals("LOOP")) {

			if (fibcEnquiry.getQuotation().getFibcLoopQuotation() == null) {
				List<FibcQuotationAttributes> fibcQuotations = new ArrayList<>();
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcQuotations.add(fibcQuotation);
				fibcEnquiry.getQuotation().setFibcLoopQuotation(fibcQuotations);
			} else {
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcEnquiry.getQuotation().getFibcLoopQuotation().add(fibcQuotation);
			}
		}

		if (status.equals("TOP-PANEL")) {
			if (fibcEnquiry.getQuotation().getFibcTopStyleQuotation() == null) {
				List<FibcQuotationAttributes> fibcQuotations = new ArrayList<>();
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcQuotations.add(fibcQuotation);
				fibcEnquiry.getQuotation().setFibcTopStyleQuotation(fibcQuotations);
			} else {
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcEnquiry.getQuotation().getFibcTopStyleQuotation().add(fibcQuotation);
			}
		}

		if (status.equals("BOTTOM-PANEL")) {
			if (fibcEnquiry.getQuotation().getFibcBottomStyleQuotation() == null) {
				List<FibcQuotationAttributes> fibcQuotations = new ArrayList<>();
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcQuotations.add(fibcQuotation);
				fibcEnquiry.getQuotation().setFibcBottomStyleQuotation(fibcQuotations);
			} else {
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcEnquiry.getQuotation().getFibcBottomStyleQuotation().add(fibcQuotation);
			}
		}

		if (status.equals("THREADING")) {
			if (fibcEnquiry.getQuotation().getFibcThreadingQuotation() == null) {
				List<FibcQuotationAttributes> fibcQuotations = new ArrayList<>();
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcQuotations.add(fibcQuotation);
				fibcEnquiry.getQuotation().setFibcThreadingQuotation(fibcQuotations);
			} else {
				FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, gsm, coatedGsm, fabSize,
						cutSize, totalMeter, totalWeight, marker, 0, 0, "");
				fibcEnquiry.getQuotation().getFibcThreadingQuotation().add(fibcQuotation);
			}
		}
	}

	public static FibcCostQuotation setCostQuotationAttributes(FibcCostQuotation fibcCostQuotation,
			String particularName, double particularRateUSD, String particularRateUnit, double costPerBagUSD,
			double costPerBagINR, double totalCost) {
		fibcCostQuotation.setParticularName(particularName);
		fibcCostQuotation.setParticularRateUSD(particularRateUSD);
		fibcCostQuotation.setParticularRateUnit(particularRateUnit);
		fibcCostQuotation.setCostPerBagUSD(costPerBagUSD);
		fibcCostQuotation.setCostPerBagINR(costPerBagINR);
		fibcCostQuotation.setTotalCost(totalCost);
		return fibcCostQuotation;
	}

	public static void fibcCostQuotation(FibcEnquiry fibcEnquiry, String particularName, double particularRateUSD,
			String particularRateUnit, double costPerBagUSD, double costPerBagINR, double totalCost) {
		if (fibcEnquiry.getQuotation().getFibcCostQuotations() == null) {
			List<FibcCostQuotation> fibcCostQuotations = new ArrayList<>();
			FibcCostQuotation fibcCostQuotation = new FibcCostQuotation();
			fibcCostQuotation = setCostQuotationAttributes(fibcCostQuotation, particularName, particularRateUSD,
					particularRateUnit, costPerBagUSD, costPerBagINR, totalCost);
			fibcCostQuotations.add(fibcCostQuotation);
			fibcEnquiry.getQuotation().setFibcCostQuotations(fibcCostQuotations);
		} else {
			FibcCostQuotation fibcCostQuotation = UPanelRules
					.getCostQuotationObject(fibcEnquiry.getQuotation().getFibcCostQuotations(), particularName);
			if (fibcCostQuotation == null) {
				List<FibcCostQuotation> fibcCostQuotations = fibcEnquiry.getQuotation().getFibcCostQuotations();
				fibcCostQuotation = new FibcCostQuotation();
				fibcCostQuotation = setCostQuotationAttributes(fibcCostQuotation, particularName, particularRateUSD,
						particularRateUnit, costPerBagUSD, costPerBagINR, totalCost);

				fibcCostQuotations.add(fibcCostQuotation);
			} else {

				setCostQuotationAttributes(fibcCostQuotation, particularName, particularRateUSD, particularRateUnit,
						costPerBagUSD, costPerBagINR, totalCost);
			}
		}
	}

	public static FibcCostQuotation getCostQuotationObject(List<FibcCostQuotation> fibcCostQuotations, String key) {
		for (FibcCostQuotation object : fibcCostQuotations) {
			if (object.getParticularName().equals(key))
				return object;
		}
		return null;
	}

	public static void fibcAccessoriesQuotation(FibcEnquiry fibcEnquiry, String particularName, double totalWeight,
			double totalCost, double totalMeter, double quantity, String marker, String imageUrl) {
		if (fibcEnquiry.getQuotation().getFibcAccessoriesQuotation() == null) {
			List<FibcQuotationAttributes> fibcPriceQuotationList = new ArrayList<>();
			FibcQuotationAttributes fibcQuotation = setQuotationAttributes(particularName, 0, 0, 0, 0, totalMeter,
					totalWeight, marker, quantity, totalCost, imageUrl);
			fibcPriceQuotationList.add(fibcQuotation);
			fibcEnquiry.getQuotation().setFibcAccessoriesQuotation(fibcPriceQuotationList);
		} else {
			FibcQuotationAttributes fibcQuotation = UPanelRules
					.getQuotationObject(fibcEnquiry.getQuotation().getFibcAccessoriesQuotation(), particularName);
			if (fibcQuotation == null) {
				List<FibcQuotationAttributes> fibcQuotationList = fibcEnquiry.getQuotation()
						.getFibcAccessoriesQuotation();
				fibcQuotation = setQuotationAttributes(particularName, 0, 0, 0, 0, totalMeter, totalWeight, marker,
						quantity, totalCost, imageUrl);
				fibcQuotationList.add(fibcQuotation);
			} else {
				fibcQuotation.setParticularName(particularName);
				fibcQuotation.setTotalCost(totalCost);
				fibcQuotation.setTotalWeight(totalWeight);
				fibcQuotation.setTotalMeter(totalMeter);
				fibcQuotation.setQuantity((int) quantity);
				fibcQuotation.setMarker(marker);
				fibcQuotation.setImageUrl(imageUrl);
			}
		}
	}

	public static void fibcPrintingQuotation(FibcEnquiry fibcEnquiry, String particularName, String description,
			String colorDes, String sideDes, double totalCost) {
		if (fibcEnquiry.getQuotation().getFibcPrintingQuotation() == null) {
			FibcPrintingQuotation fibcPrintingQuotation = new FibcPrintingQuotation();
			fibcPrintingQuotation.setParticularName(particularName);
			fibcPrintingQuotation.setDescription(description);
			fibcPrintingQuotation.setColorDescription(colorDes);
			if (sideDes == null) {
				fibcPrintingQuotation.setSideDescription("--");
			}
			fibcPrintingQuotation.setSideDescription(sideDes);
			fibcPrintingQuotation.setTotalCost(totalCost);
			fibcEnquiry.getQuotation().setFibcPrintingQuotation(fibcPrintingQuotation);
		} else {
			FibcPrintingQuotation fibcPrintingQuotation = fibcEnquiry.getQuotation().getFibcPrintingQuotation();
			fibcPrintingQuotation.setParticularName(particularName);
			fibcPrintingQuotation.setDescription(description);
			fibcPrintingQuotation.setColorDescription(colorDes);
			if (sideDes == null) {
				fibcPrintingQuotation.setSideDescription("--");
			}
			fibcPrintingQuotation.setSideDescription(sideDes);
			fibcPrintingQuotation.setTotalCost(totalCost);

		}
	}

	public static FibcPrintingQuotation getPrintingQuotationObject(List<FibcPrintingQuotation> priceQuotations,
			String key) {
		for (FibcPrintingQuotation object : priceQuotations) {
			if (object.getParticularName().equals(key))
				return object;
		}
		return null;
	}

	public static void fibcLinerQuotation(FibcEnquiry fibcEnquiry, String particularName, String linerShape,
			String linerType, String linerAttachmentType, String linerAttachmentStyle, String linerColor,
			String linerShade) {
		FibcLinerQuotation fibcLinerQuotation;
		if (fibcEnquiry.getQuotation().getFibcLinerQuotation() == null) {
			fibcLinerQuotation = new FibcLinerQuotation();
		} else {
			fibcLinerQuotation = fibcEnquiry.getQuotation().getFibcLinerQuotation();
		}
		fibcLinerQuotation.setParticularName(particularName);
		fibcLinerQuotation.setLinerShape(linerShape);
		fibcLinerQuotation.setLinerType(linerType);
		fibcLinerQuotation.setLinerAttachmentStyle(linerAttachmentStyle);
		fibcLinerQuotation.setLinerAttachmentType(linerAttachmentType);
		fibcLinerQuotation.setLinerColor(linerColor);
		fibcLinerQuotation.setLinerShade(linerShade);
		fibcEnquiry.getQuotation().setFibcLinerQuotation(fibcLinerQuotation);

	}
}
