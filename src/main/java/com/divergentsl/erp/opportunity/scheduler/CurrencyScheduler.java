package com.divergentsl.erp.opportunity.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import com.divergentsl.erp.opportunity.mongodb.documents.CurrencyDetails;
import com.divergentsl.erp.opportunity.services.OpportunityService;

public class CurrencyScheduler {

	@Autowired
	private OpportunityService salesService;

	@Scheduled(cron = "0 0 11 * * *")
	public void execute() {
		final String uri = "http://api.fixer.io/latest";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		CurrencyDetails currencyDetails = new CurrencyDetails();
		currencyDetails.setCurrencyRate(result);
		salesService.saveCrrency(currencyDetails);	
	}
}
