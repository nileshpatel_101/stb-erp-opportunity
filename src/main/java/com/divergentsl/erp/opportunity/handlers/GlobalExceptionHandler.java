package com.divergentsl.erp.opportunity.handlers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.divergentsl.erp.opportunity.domains.ErrorBean;
import com.divergentsl.erp.opportunity.exceptions.OpportunityValidationException;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(OpportunityValidationException.class)
	public ResponseEntity<ErrorBean> handleBusinessValidationException(HttpServletRequest request,
			OpportunityValidationException ex) {
		if(ex.getMessage().equals("UserID")){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else if(ex.getMessage().equals("Username")){
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}else{
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
