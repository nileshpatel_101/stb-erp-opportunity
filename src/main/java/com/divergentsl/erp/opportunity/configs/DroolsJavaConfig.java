package com.divergentsl.erp.opportunity.configs;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("javaconfig")
@Configuration
@ComponentScan(basePackages = { "org.kie.spring.annotations" })
public class DroolsJavaConfig {}