package com.divergentsl.erp.opportunity.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;

/**
 * Created by Vandana on 12/6/2017.
 */
@Profile("xml")
@Configuration
@ImportResource("classpath:kmodule.xml")
public class DroolsXmlNamespaceConfig {

}